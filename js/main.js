$(document).ready(function(){
    new_view_project(0);
    $(function() {
	$('#images > div').each(function() {
		var $cfs = $(this);
		$cfs.carouFredSel({
			direction: 'up',
			circular: false,
			infinite: false,
			auto: false,
			scroll: {
				queue: 'last'
			},
			items: {
				visible: 1,
				width: 235,
				height: 235
			}
		});
		$cfs.hover(
			function() {
				$cfs.trigger('next');
			},
			function() {
				$cfs.trigger('prev');
			}
		);
	});
    });

    function delSold (){
        $('#sold').removeClass('animated tada');
    }
    $('#sold').hover(function(){
        $(this).addClass('animated tada')
    });
    $('#sold').mouseleave(function(){
        setTimeout(delSold,250)
    });


    //при нажатии на ссылку производим ряд анимаций
    function delRub(){
        $('#farsh_not_die').removeClass('animated rubberBand');
    }
    var i = 0;
    $('#press_me_rotate, #press_me_rotate_to').click(function(){
            if(i==0) {
                $('#ru4ka').addClass('rotated');
                $('#amazing_text').addClass('animated zoomOutDown');
                $('#shape_1').addClass('fadeInDownBig');
                $('#shape_1').show();
                $('#shape_2').addClass('fadeInDownBig');
                $('#shape_2').show();
                $('#shape_3').addClass('fadeInDownBig');
                $('#shape_3').show();
                $('#shape_4').addClass('fadeInDownBig');
                $('#shape_4').show();
                $('#shape_5').addClass('fadeInDownBig');
                $('#shape_5').show();
                i++;
            }else if(i!=0){
                $('#farsh_not_die').addClass('animated rubberBand');
                setTimeout(delRub,1000);
            }

        console.log(i);
    });
    $(document).ready(function(){
    $('[animate]').css('opacity','0');
   // $('.container > *').attr('animate','a')
    gagen();
});
$(window).scroll(function(){
    gagen();
});
function gagen()
{
    $('[animate]').each(function(){  
        if ( !$(this).hasClass('active') ) {
            if ( $(window).scrollTop()+$(window).height()>$(this).offset().top ) {
                $(this).addClass('active');
                $(this).addClass('animated '+$(this).attr('animate'));
            }
        }
        //else {
           // if ( $(window).scrollTop()+$(this).height()<$(this).offset().top ) {
           //     $(this).removeClass('active');
            //    $(this).removeClass('animated '+$(this).attr('animate'));
        //    }
        //}
    });    
}
});

$(window).resize(function(){


});

function new_view_project(i){
    var j = 0;
    $('.view_project').each(function(){

        var number_animation = 1;
        var end_animation = 0;
        var this_hover = $(this);
        if( j >= i ){
            $(this).hover(function(){
                this_hover.addClass('hover');
                setTimeout(function(_this){
                    if( !_this.hasClass('active_animate') && _this.hasClass('hover') ){

                        _this.find('.detail_project').animate({
                            'top': 0
                        }, 500);

                    }
                }, 500, this_hover)

            }, function(){
                var name_project = $(this).find('.name_project');
                this_hover.removeClass('hover');
                $(this).find('.detail_project').animate({
                    'top': 244
                }, 500);
            });
        }
        j++;

    });
    window.for_need_project = j;
}
