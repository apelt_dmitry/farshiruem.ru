<?php

/**
 * This is the model class for table "portfolio".
 *
 * The followings are the available columns in table 'portfolio':
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $name
 */
class Portfolio extends CActiveRecord
{
    
        public $image;
	public function tableName()
	{
            return 'portfolio';
	}


	public function rules()
	{
            return array(
                array('title, name', 'required'),
                array('title, name', 'length', 'max'=>32),
                array('date', 'length', 'max'=>16),
                array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true, 'maxFiles'=>12),
                
                // search
                array('id, title, date, name', 'safe', 'on'=>'search'),
            );
	}


	public function relations()
	{
            return array(
                'portfolioSlide'=>array(self::HAS_MANY, 'PortfolioSlide', 'id_portfolio','order'=>'`position` ASC, `id` ASC'),
            );
	}


	public function attributeLabels()
	{
            return array(
                'image[]' => 'Изображение',
                'id' => 'ID',
                'title' => 'Title',
                'date' => 'Date',
                'name' => 'Name',
            );
	}


	public function search()
	{
            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);         
            $criteria->compare('title',$this->title,true);         
            $criteria->compare('date',$this->date,true);         
            $criteria->compare('name',$this->name,true);         

            $dataProvider = new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>20,
                ),
            ));

            $dataProvider->sort->defaultOrder = '`id` DESC';

            return $dataProvider;
	}


	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
        public function get_preview()
        {
            
            $slides=$this->portfolioSlide;
            if (count($slides)==0){
                return NULL;
            }
            else {
                $result='<ul class="sortable">';
                foreach ( $slides as $slide){
                    $result.='<li>'.BsHtml::image('/uploads/portfolio/preview/'.$slide->preview, null, array('data-id'=>$slide->id, 'id_portfolio'=>$slide->id_portfolio)).'</li>' ;
                }
                return $result.'</ul>';
            }
        }
        
}
