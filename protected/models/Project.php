<?php

/**
 * This is the model class for table "project".
 *
 * The followings are the available columns in table 'project':
 * @property integer $id
 * @property string $name_ru
 * @property string $description_view_ru
 * @property string $description_detail_ru
 * @property string $image_view
 * @property string $image
 * @property integer $type
 */
class Project extends CActiveRecord
{
    public $_image;
    public $_image_view;

	public function tableName()
	{
            return 'project';
	}


	public function rules()
	{
            return array(
                array('name_ru, type', 'length', 'max'=>255),
                array('image_view, sub_name, date, image', 'length', 'max'=>32),
                array('sort', 'numerical', 'integerOnly' => true),
                array('_image, _image_view', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true),
            
                // search
                    array('id, name_ru, description_view_ru, description_detail_ru, sort, sub_name, date, image_view, image, type', 'safe'),
            );
	}


	public function relations()
	{
            return array(
            );
	}


	public function attributeLabels()
	{
            return array(
                'id' => 'ID',
                'name_ru' => 'Название',
                'description_view_ru' => 'Описание',
                'description_detail_ru' => 'Детальное описание',
                'image_view' => 'Логотип',
                'image' => 'Изображение сайта',
                '_image_view' => 'Логотип',
                '_image' => 'Изображение сайта',
                'type' => 'Тип сайта',
                'sort' => 'Номер',
                'date' => 'Дата',
                'sub_name' => 'Доп. имя'

            );
	}


	public function search()
	{
            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);         
            $criteria->compare('name_ru',$this->name_ru,true);         
            $criteria->compare('description_view_ru',$this->description_view_ru,true);         
            $criteria->compare('description_detail_ru',$this->description_detail_ru,true);
            $criteria->compare('date',$this->date,true);
            $criteria->compare('sub_name',$this->sub_name,true);
            $criteria->compare('image_view',$this->image_view,true);         
            $criteria->compare('image',$this->image,true);         
            $criteria->compare('type',$this->type);
            $criteria->compare('sort',$this->sort);

        $dataProvider = new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>100,
                ),
            ));

            $dataProvider->sort->defaultOrder = '`id` DESC';

            return $dataProvider;
	}


	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
    public function getImageView(){
        return BsHtml::image('/uploads/project/view/preview/'.$this->image_view);
    }

    public function getTypeSite(){
        switch( $this->type ){
            case 0:
                return 'Не выбран';
            case 1:
                return 'Landing page';
            case 2:
                return 'Web разработка';
            case 3:
                return 'Разработка ПО';
        }
    }
}
