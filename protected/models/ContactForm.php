<?php

/**
 * This is the model class for table "contactForm".
 *
 * The followings are the available columns in table 'contactForm':
 * @property integer $id
 * @property string $ip
 * @property string $name
 * @property string $phone
 * @property string $commit
 * @property integer $date
 */
class ContactForm extends CActiveRecord
{
    

	public function tableName()
	{
            return 'contactForm';
	}


	public function rules()
	{
            return array(
                array('name, phone', 'required'),
                array('date', 'numerical', 'integerOnly'=>true),
                array('ip, phone', 'length', 'max'=>32),
                array('name, theme', 'length', 'max'=>64),
                array('commit', 'length', 'max'=>255),
            
                // search
                    array('id, ip, name, theme, phone, commit, date', 'safe', 'on'=>'search'),
            );
	}


	public function relations()
	{
            return array(
            );
	}


	public function attributeLabels()
	{
            return array(
                'id' => 'ID',
                'ip' => 'Ip',
                'name' => 'Name',
                'phone' => 'Phone',
                'commit' => 'Commit',
                'date' => 'Date',
                'theme' => 'Тема',
             );
	}


	public function search()
	{
            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);         
            $criteria->compare('ip',$this->ip,true);         
            $criteria->compare('name',$this->name,true);         
            $criteria->compare('phone',$this->phone,true);         
            $criteria->compare('commit',$this->commit,true);
            $criteria->compare('date',$this->date);
            $criteria->compare('theme',$this->theme);

            $dataProvider = new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>20,
                ),
            ));

            $dataProvider->sort->defaultOrder = '`id` DESC';

            return $dataProvider;
	}


	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
        public function goContact()
        {
            $message = [];

            if( $this->name != '' ){
                $message[] = 'Отправитель: '.$this->name;
            }
            if( $this->phone != '' ){
                $message[] = 'Телефон: '.$this->phone;
            }
            if( $this->commit != '' ){
                $message[] = 'Комментарий: '.$this->commit;
            }

            include_once "libmail.php";
            $m = new Mail;
            $m->smtp_on('ssl://smtp.gmail.com', Yii::app()->controller->config->mail_order, Yii::app()->controller->config->password_mail_order, '465');
            $m->From($_SERVER['HTTP_HOST'].';'.Yii::app()->controller->config->mail_order);
            $m->To(array(Yii::app()->controller->config->manager_order_mail));
            $m->Subject($this->theme);
            $m->Body(implode('<br/>', $message),'html');
            $m->Send();
        }
        
}
