<?php

/**
 * This is the model class for table "User".
 *
 * The followings are the available columns in table 'User':
 * @property integer $id
 * @property string $login
 * @property string $password
 */
class User extends CActiveRecord
{

    private $_identity;

	public function tableName()
	{
            return 'user';
	}


	public function rules()
	{
            return array(
                // login
                    array('login, password', 'required', 'on'=>'login'),
                    array('password', 'authenticate', 'on'=>'login'),
            
                // search
                    array('id, login, password', 'safe', 'on'=>'search'),
            );
	}

// on auto
    public function authenticate($attribute, $params)
    {
        if( !$this->hasErrors() ) {
            $this->_identity = new UserIdentity($this->login, $this->password);
            if( !$this->_identity->authenticate() ) {
                $this->addError('password', 'Неверный логин или пароль.');
            }
        }
    }

    public function login()
    {
        if ( $this->_identity===null ) {
            $this->_identity=new UserIdentity($this->login, $this->password);
            $this->_identity->authenticate();
        }
        if ( $this->_identity->errorCode===UserIdentity::ERROR_NONE ) {
            $duration = 3600*24*30;
            Yii::app()->user->login($this->_identity, $duration);
            /*User::model()->updateByPk(Yii::app()->user->id, array(
                'date_last_login'=>time(),
            ));*/
            return true;
        }
        else {
            return false;
        }
    }
// end auto


	public function relations()
	{
            return array(
            );
	}


	public function attributeLabels()
	{
            return array(
                'id' => 'ID',
                'login' => 'Логин',
                'password' => 'Пароль',
            );
	}


	public function search()
	{
            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);         
            $criteria->compare('login',$this->login,true);         
            $criteria->compare('password',$this->password,true);         

            $dataProvider = new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>20,
                ),
            ));

            $dataProvider->sort->defaultOrder = '`id` DESC';

            return $dataProvider;
	}


	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
        
        
}
