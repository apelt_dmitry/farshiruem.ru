<?php

/**
 * This is the model class for table "city".
 *
 * The followings are the available columns in table 'city':
 * @property integer $id
 * @property string $name
 * @property string $phone
 */
class City extends CActiveRecord
{
    

	public function tableName()
	{
            return 'city';
	}


	public function rules()
	{
            return array(
                array('name, phone', 'required'),
                array('name', 'length', 'max'=>32),
                array('phone', 'length', 'max'=>16),
            
                // search
                    array('id, name, phone', 'safe', 'on'=>'search'),
            );
	}


	public function relations()
	{
            return array(
            );
	}


	public function attributeLabels()
	{
            return array(
                'id' => 'ID',
                'name' => 'Город',
                'phone' => 'Телефон',
            );
	}


	public function search()
	{
            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);         
            $criteria->compare('name',$this->name,true);         
            $criteria->compare('phone',$this->phone,true);         

            $dataProvider = new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>20,
                ),
            ));

            $dataProvider->sort->defaultOrder = '`id` DESC';

            return $dataProvider;
	}


	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
        
        
}
