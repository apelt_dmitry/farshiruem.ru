<?php

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property integer $id
 * @property string $adminEmail
 * @property string $yandex
 */
class Config extends CActiveRecord
{
    

	public function tableName()
	{
            return 'config';
	}


	public function rules()
	{
            return array(
                array('yandex', 'required'),
                array('mail_order, password_mail_order, manager_order_mail', 'length', 'max'=>128),
                array('vk, facebook, instagram', 'length', 'max'=>255),
                // search
                    array('id, vk, facebook, instagram, yandex, mail_order, password_mail_order, manager_order_mail', 'safe', 'on'=>'search'),
            );
	}


	public function relations()
	{
            return array(
            );
	}


	public function attributeLabels()
	{
            return array(
                'id' => 'ID',
                'mail_order' => 'Логин',
                'password_mail_order' => 'Пароль',
                'manager_order_mail' => 'email',
                'yandex' => 'Yandex',
                'vk' => 'Vk',
                'facebook' => 'Facebook',
                'instagram' => 'Instagram',
            );
	}


	public function search()
	{
            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);
            $criteria->compare('mail_order',$this->mail_order,true);
            $criteria->compare('password_mail_order',$this->password_mail_order,true);
            $criteria->compare('manager_order_mail',$this->manager_order_mail,true);
            $criteria->compare('yandex',$this->yandex,true);
            $criteria->compare('vk',$this->vk,true);
            $criteria->compare('facebook',$this->facebook,true);
            $criteria->compare('instagram',$this->instagram,true);

        $dataProvider = new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>20,
                ),
            ));

            $dataProvider->sort->defaultOrder = '`id` DESC';

            return $dataProvider;
	}


	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
        
        
}
