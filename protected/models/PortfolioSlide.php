<?php

/**
 * This is the model class for table "portfolio_slide".
 *
 * The followings are the available columns in table 'portfolio_slide':
 * @property integer $id
 * @property integer $id_portfolio
 * @property string $preview
 * @property integer $position
 */
class PortfolioSlide extends CActiveRecord
{
    
        public $image;
	public function tableName()
	{
            return 'portfolio_slide';
	}


	public function rules()
	{
            return array(
                array('id_portfolio, preview', 'required'),
                array('id_portfolio, position', 'numerical', 'integerOnly'=>true),
                array('preview', 'length', 'max'=>64),
                array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true, 'maxFiles'=>12),
                
                // search
                    array('id, id_portfolio, preview, position', 'safe', 'on'=>'search'),
            );
	}


	public function relations()
	{
            return array(
            );
	}


	public function attributeLabels()
	{
            return array(
                'image[]' => 'Изображения',
                'id' => 'ID',
                'id_portfolio' => 'Id Portfolio',
                'preview' => 'Preview',
                'position' => 'Position',
            );
	}


	public function search()
	{
            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);         
            $criteria->compare('id_portfolio',$this->id_portfolio);         
            $criteria->compare('preview',$this->preview,true);         
            $criteria->compare('position',$this->position);         

            $dataProvider = new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>20,
                ),
            ));

            $dataProvider->sort->defaultOrder = '`id` DESC';

            return $dataProvider;
	}


	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
        
        
}
