<?php

/**
 * This is the model class for table "meta_tags".
 *
 * The followings are the available columns in table 'meta_tags':
 * @property integer $id
 * @property string $meta_keywords
 * @property string $meta_title
 * @property string $meta_description
 * @property string $name_page
 */
class MetaTags extends CActiveRecord
{
    

	public function tableName()
	{
            return 'meta_tags';
	}


	public function rules()
	{
            return array(
                array('name_page', 'required'),
                array('name_page', 'length', 'max'=>64),
                array('meta_keywords, meta_title, meta_description', 'safe'),
            
                // search
                    array('id, meta_keywords, meta_title, meta_description, name_page', 'safe', 'on'=>'search'),
            );
	}


	public function relations()
	{
            return array(
            );
	}


	public function attributeLabels()
	{
            return array(
                'id' => 'ID',
                'meta_keywords' => 'Meta Keywords',
                'meta_title' => 'Meta Title',
                'meta_description' => 'Meta Description',
                'name_page' => 'Name Page',
            );
	}


	public function search()
	{
            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);         
            $criteria->compare('meta_keywords',$this->meta_keywords,true);         
            $criteria->compare('meta_title',$this->meta_title,true);         
            $criteria->compare('meta_description',$this->meta_description,true);         
            $criteria->compare('name_page',$this->name_page,true);         

            $dataProvider = new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>20,
                ),
            ));

            $dataProvider->sort->defaultOrder = '`id` DESC';

            return $dataProvider;
	}


	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
        
        
}
