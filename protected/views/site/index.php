<?php
Yii::app()->name = $meta->meta_title;
Yii::app()->clientScript->registerMetaTag($meta->meta_description, 'description');
Yii::app()->clientScript->registerMetaTag($meta->meta_keywords, 'keywords');
$config = Config::model()->findByPk(1);
?>
<script>
    $(document).ready(function () {
        //при изменении города мы меняем номера телефонов в header и footer
        function getPhone(city) {
            $.ajax({
                url: '/site/_ajax?action=id',
                type: 'POST',
                dataType: 'json',
                data: {
                    id: city
                },
                success: function (data) {
                    $('.phone').html(data);
                    $('#top_footer_phone').html(data);
                }
            });
        };
        if($.cookie( 'CookieCity') != null){
            $('#city').val($.cookie( 'CookieCity'));
            $('#city').change();
            getPhone($('#city').val());
            if ($('#city').children().eq(0).text() == 'Ваш город...') {
                $('#city').children().eq(0).remove();
            }
        }

        $('#city').change(function () {
            var city;
            getPhone($('#city').val());

            $.cookie( 'CookieCity' , $(this).val() );

            if ($('#city').children().eq(0).text() == 'Ваш город...') {
                $('#city').children().eq(0).remove();
            }
        });

        $('#city2').change(function () {
            var thisVal = $('#city2 option[value="' + $('#city2').val() + '"]').text();
            $('#selectcity').html(thisVal);
            $('#city').val($(this).val());
            $('#city').change();
        });


        $(".under_header a").click(function () {
            var selected = $(this).attr('href');
            $.scrollTo(selected, 1500);
            return false;
        });


        // при пустой ссылке на соц сеть мы скрываем иконку
        if ($('.vk').attr('href') == '') {
            $('.vk').css('display', 'none');
        };
        if ($('.f').attr('href') == '') {
            $('.f').css('display', 'none');
        };
        if ($('.inst').attr('href') == '') {
            $('.inst').css('display', 'none');
        };

    });
</script>
<div id="main_content">
    <div class="header">
        <div class="container">
            <a href="/" class="logo">
                <div class="logo_text">
                    Федеральное агентство рекламных решений
                </div>
            </a>

            <div class="city_social">
                <a href="<?= $config->vk ?>" target="_blank" class="vk"></a>
                <a href="<?= $config->facebook ?>" target="_blank" class="f"></a>
                <a href="<?= $config->instagram ?>" target="_blank" class="inst"></a>

                <div class="city_selector top_city_box">
                    <?php echo CHtml::dropDownList('city', null, CHtml::listData($city, 'id', 'name'), array('empty' => 'Ваш город...', 'class' => 'styled')); ?>
                </div>
            </div>
            <div class="phone_box">
                <div class="phone"><?php echo City::model()->findByPk(1)->phone ?></div>
                <a href="#lptracker" class="under_phone">заказать звонок</a>
            </div>
        </div>
    </div>
    <div class="under_header">
        <div class="container">
            <div id="ak"></div>
            <div id="brains"></div>
            <div id="farsh_not_die"></div>
            <div id="myasorubka"></div>
            <div id="amazing_text"></div>
            <a href="#" id="press_me_rotate_to"></a>

            <div id="ru4ka"></div>
            <a href="#" id="press_me_rotate"></a>

            <div id="ellips"></div>
            <div id="rotate"></div>
            <div id="sold"></div>
            <a href="#" id="button_sold" onclick="$('#contactDialog').dialog('open'); return false;"></a>

            <div id="tarelka_1"></div>
            <div id="shape_1" class="animated"></div>
            <div id="tarelka_2"></div>
            <div id="shape_2" class="animated"></div>
            <div id="tarelka_3"></div>
            <div id="shape_3" class="animated"></div>
            <div id="tarelka_4"></div>
            <div id="shape_4" class="animated"></div>
            <div id="tarelka_5"></div>
            <div id="shape_5" class="animated"></div>
            <a href="#int_marc" class="base link_1">Интернет маркетинг</a>
            <a href="#neiro" class="base link_2">нейромаркетинг</a>
            <a href="#brand" class="base link_3">Брендинг</a>
            <a href="#strat" class="base link_4">стратегия бизнеса</a>
            <a href="#get_bizz" class="base link_5">Готовый бизнес</a>
        </div>
    </div>
    <div id="this_is">
        <div class="container">
            <div id="border_this_is"></div>
            <div id="this_is_box_text">
                <div id="meat">
                    Ф.А.Р.Ш.
                </div>
                <div id="it">
                    - ЭТО
                </div>
            </div>
            <div class="this_base_box">
                <div class="this_base">
                    <img src="/img/this_clock.png"/>

                    <p class="this_base_title">Быстро</p>

                    <p class="this_base_description">24 часа online</p>
                </div>
                <div class="this_base">
                    <img src="/img/this_check.png"/>

                    <p class="this_base_title">Просто</p>

                    <p class="this_base_description">Полный цикл <br/> рекламных услуг</p>
                </div>
                <div class="this_base">
                    <img src="/img/this_lamp.png"/>

                    <p class="this_base_title">Вкусно</p>

                    <p class="this_base_description">Только авторская кухня</p>
                </div>
                <div class="this_base">
                    <img src="/img/this_pig.png"/>

                    <p class="this_base_title">Доступно</p>

                    <p class="this_base_description">Гибкая система оплаты</p>
                </div>
            </div>
        </div>
    </div>
    <a id="int_marc"></a>

    <div id="menu_bgr">
        <div id="menu_head">
            <div id="menu_head_line"></div>
            <div id="menu_head_menu">Меню</div>
        </div>

        <div class="menu_box">

            <img src="/img/menu_1.png"/>

            <div class="menu_box_title">
                Интернет маркетинг
            </div>
            <a href="#" class="button_order" onclick="$('#contactDialog').dialog('open'); return false;"></a>

            <div class="sostav">
                состав
            </div>
            <div class="under_sostav">
                <ul>
                    <li type="disc">Разработка сайтов, лендингов,<br/> интернет-магазинов.</li>
                    <li type="disc">Поддержка и обслуживание сайтов.</li>
                    <li type="disc">Рекламные кампании в интернете.</li>
                    <li type="disc">SEO оптимизация.</li>
                </ul>
            </div>
            <div class="menu_footer">
                <ul>
                    <li type="disc"> SMM продвижение.</li>
                    <li type="disc"> Разработка мобильных приложений: дизайн интерфейсов.</li>
                    <li type="disc"> Разработка репутации в сети.</li>
                </ul>
            </div>
            <a id="neiro"></a>

            <div class="menu_footer_line"></div>
        </div>

        <div class="menu_box">

            <img src="/img/menu_2.png"/>

            <div class="menu_box_title">
                Нейромаркетинг<br/>
                <span>(Маркетинг + ПОДСОЗНАНИЕ)</span>
            </div>
            <a href="#" class="button_order" onclick="$('#contactDialog').dialog('open'); return false;"></a>

            <div class="sostav">
                состав
            </div>
            <div class="under_sostav us_2">
                <span>
                    Создание концепций заведений,<br/>
                    брендов и ТМ.
                </span>
            </div>
            <div class="menu_footer">
                <span>
                    Тестирование методом ZMET:
                </span>
                <ul>
                    <li type="disc">обонятельные раздражители: аромамаркетинг.</li>
                    <li type="disc">визуальные раздражители: образы, цвет, элементы оформления, текст.</li>
                    <li type="disc">аудиальные раздражители: звуки, стиль музыкального оформления.</li>
                    <li type="disc">тактильные раздражители: подбор материалов обивки мебели, текстуры стен,</li>
                    <li type="disc"> материалы для оформления декоративных элементов, пола, лестниц, поручней,
                        полиграфии.
                    </li>
                </ul>
                <span2>
                    Решения для ритейла, торговых центров:
                </span2>
                <ul>
                    <li type="disc">Создание и тестирование концепций для торговых сетей</li>
                    <li type="disc">Интерьер: дизайн проект, коррекция ошибок, рекомендации к оформлению, фирменный
                        стиль
                    </li>
                    <li type="disc">Тестирование при помощи Eye tracker - выявление проблем маркетинговой коммуникации,
                        составление рекомендаций
                    </li>
                    <li type="disc">Эмоциональный мерчендайзинг.</li>
                </ul>
                <span2>
                    Решения для интернета:
                </span2>
                <ul>
                    <li type="disc">Тестирование при помощи Eye tracker - выявление проблем маркетинговой коммуникации,
                        составление рекомендаций, подбор цветового решения для сайтов и соц сетей.
                    </li>
                    <li type="disc">Определение целевой аудитории сайта, формирование управляющей идеи</li>
                    <li type="disc">Разработка структуры, прототипа и дизайна сайта.</li>
                </ul>
            </div>
            <a id="brand"></a>

            <div class="menu_footer_line"></div>
        </div>
        <div class="menu_box">

            <img src="/img/menu_3.png"/>

            <div class="menu_box_title">
                брендинг
            </div>
            <a href="#" class="button_order" onclick="$('#contactDialog').dialog('open'); return false;"></a>

            <div class="sostav">
                состав
            </div>
            <div class="under_sostav">
                <ul>
                    <li type="disc">Разработка названия (нейминг) и<br/> бренд-лайна (слоган)</li>
                    <li type="disc">Разработка логотипа</li>
                    <li type="disc">Разработка креативной концепции бренда</li>
                    <li type="disc">Оригинальный дизайн упаковки, этикетки.</li>
                    <li type="disc">Разработка бренд-бука, дизайн фирменной<br/> полиграфии.</li>
                </ul>
            </div>
            <div class="menu_footer" id="brending"></div>
            <a id="strat"></a>

            <div class="menu_footer_line"></div>
        </div>

        <div class="menu_box">

            <img src="/img/menu_4.png"/>

            <div class="menu_box_title">
                стратегия бизнеса
            </div>
            <a href="#" class="button_order" onclick="$('#contactDialog').dialog('open'); return false;"></a>

            <div class="sostav">
                состав
            </div>
            <div class="under_sostav">
                <ul>
                    <li type="disc">Разработка характера и образа бренда.</li>
                    <li type="disc">Формирование уникального торгового<br/> предложения компании, фирмы.</li>
                    <li type="disc">Разработка коммуникационной стратегии</li>
                    <li type="disc">Разработка креативной концепции<br/> рекламной кампании</li>
                </ul>
            </div>
            <div class="menu_footer">
                <ul>
                    <li type="disc">Разработка видео-роликов</li>
                    <li type="disc">Разработка аудио-роликов и джинглов</li>
                </ul>
            </div>
            <a id="get_bizz"></a>

            <div class="menu_footer_line"></div>
        </div>

        <div class="menu_box">

            <img src="/img/menu_5.png"/>

            <div class="menu_box_title">
                готовый бизнес
            </div>
            <a href="#" class="button_order" onclick="$('#contactDialog').dialog('open'); return false;"></a>

            <div class="sostav">
                состав
            </div>
            <div class="under_sostav">
                <ul>
                    <li type="disc">Создание, «упаковка» под франшизу.</li>
                    <li type="disc">Разработка специальных мероприятий по<br/> работе с персоналом компаний при<br/>
                        внедрении нового бренда и ребрендинге.
                    </li>
                    <li type="disc">Обучение персонала по авторской методике<br/> (ИНТЕНСИВ-курс).</li>
                </ul>
            </div>
            <div class="menu_footer">
                <ul>
                    <li type="disc">Брендирование торгового оборудования, формы персонала, транспорта</li>
                    <li type="disc">Разработка конструкций для экстерьера и интерьера: стенды, детские площадки, стелы
                    </li>
                    <li type="disc">Разработка концепт-бука</li>
                    <li type="disc">Авторский надзор при имплементации айдентики в точки продаж, офисы</li>
                </ul>
            </div>
            <div class="menu_footer_line"></div>
        </div>
    </div>

    <div class="container">
        <div id="portfolio_title_box">
            <div id="left_portfolio_line"></div>
            <div id="portfolio_title">
                Портфолио
            </div>
            <div id="right_portfolio_line"></div>
        </div>
    </div>

    <div class="portfolio_slider_box">
        <!--<div class="container">
            <div id="wrapper">
                <div id="images">
                    <?php foreach ($portfolios as $portfolio): ?>
                    <div>
                       
                        
                        <?php
            $all_image = $portfolio->portfolioSlide;
            $i = 1;
            foreach ($all_image as $slide) {
                if ($i == 1) {
                    echo '<img  src="/uploads/portfolio/preview/' . $slide->preview . '"  width="275" height="200" />';
                } else {
                    //echo '<img src="/uploads/portfolio/preview/'.$slide->preview.'" style="display:none" width="275" height="200" />';
                }
                $i++;
            }

            ?>
                        
                        <span>
                            <?php
            foreach ($all_image as $slide) {
                if ($i == 1) {
                    $slide_first = $slide->preview;
                    echo '<a rel="portfolio_' . $portfolio->id . '" href="/uploads/portfolio/' . $slide->preview . '" style="display:none" class="gallery"><img rell="portfolio_' . $portfolio->id . '" src="/uploads/portfolio/' . $slide->preview . '"/></a>';
                } else {
                    echo '<a rel="portfolio_' . $portfolio->id . '" href="/uploads/portfolio/' . $slide->preview . '" style="display:none" class="gallery"><img rell="portfolio_' . $portfolio->id . '" src="/uploads/portfolio/' . $slide->preview . '"/></a>';
                }
                $i++;
            }
            ?>
                            <p class="portfolio_title">
                                <?= $portfolio->title ?>
                            </p>
                            <p class="portfolio_date">
                                <?= $portfolio->date ?>
                            </p>
                            <p class="portfolio_firm">
                                <?= $portfolio->name ?>
                            </p> 
                            <?php
            foreach ($all_image as $slide) {
                if ($i !== 1) {
                    echo '<a rel="portfolio_' . $portfolio->id . '" href="/uploads/portfolio/' . $slide->preview . '" class="gallery button_portfolio"></a>';
                } else {
                    echo 'wtf!!!';
                }
            }
            ?>
                            
                        </span>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>-->
        <div class="container">
            <div id="our_project">
                <?php $i = 0;
                foreach ($project as $val): if ($i % 4 == 0) {
                    $ml = '';
                } else {
                    $ml = 'ml60';
                } ?>

                    <div class="view_project  <?= $ml; ?>" project="<?= $i; ?>">
                        <img src="/uploads/project/view/<?= $val->image_view ?>">

                        <div class="name_project" project="<?= $i; ?>"><?= $val->name_ru; ?></div>
                        <div class="date_project" project="<?= $i; ?>"><?= $val->date; ?></div>
                        <div class="sub_name_project" project="<?= $i; ?>"><?= $val->sub_name; ?></div>
                        <div class="detail_project" project="<?= $i; ?>">

                            <div class="name_icon"><?= $val->name_ru; ?></div>

                            <div class="date_icon"><?= $val->date; ?></div>

                            <div class="sub_name_icon"><?= $val->sub_name; ?></div>

                            <a href="#view<?= $val->id; ?>" rel="1" class="modal_view"><?= Yii::t('e', ''); ?></a>
                        </div>
                    </div>

                    <div id="view<?= $val->id; ?>" class="modal_view_project">
                        <div class="modal_name_project"><?= $val->name_ru; ?><br/></div>

                        <div><?= $val->description_detail_ru ?></div>
                        <div><img data_src="/uploads/project/site/<?= $val->image ?>" class="timeout_img"
                                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC">
                        </div>
                        <div class="exit_fancy"></div>
                        <div class="top_fancy"></div>
                        <div class="new-next"></div>
                        <div class="new-prev"></div>
                    </div>

                    <?php $i++; endforeach; ?>
            </div>
        </div>


        <script>
            $(document).ready(function () {
                $("a.modal_view").fancybox({
                    'padding': 0,
                    'autoResize': false,
                    'autoHeight': false,
                    'scrolling': 'no',
                    'scrollOutside': true,
                    'nextEffect': 'fade',
                    'prevEffect': 'fade',
                    'nextSpeed': 600,
                    'prevSpeed': 600,
                    'arrows': false,
//                    'afterLoad'        :   function(){
//                        setTimeout(function(){
//                            $('body').animate({
//                                'scrollTop': 0
//                            },500);
//                            $('html').animate({
//                                'scrollTop': 0
//                            },500);
//                        }, 500)
//                    },
                    'onUpdate': function () {
                        check_visible_img();
                        position_button();
                        //smart_ajax();
                    }
                });

                function check_visible_img() {
                    $('.timeout_img').each(function () {
                        if ($(this).is(':visible')) {
                            $(this).attr('src', $(this).attr('data_src'));
                        }
                    })
                }


                function position_button() {
                    var pos_x = ($(document).width() - 733) / 2;
                    if ($(window).width() < 900) {
                        $('.top_fancy').hide();
                        $('.exit_fancy').hide();
                        $('.new-prev').hide();
                        $('.new-next').hide();
                    }
                    else {
                        $('.new-prev').css('left', pos_x - 60 - 15).show();
                        $('.new-next').css('right', pos_x - 60 - 15).show();
                        $('.top_fancy').css('right', pos_x - 60 - 15).show();
                        $('.exit_fancy').css('right', pos_x - 60 - 10 - 53 - 15).show();
                    }

                }

                click_button();
                function click_button() {
                    $('.new-prev').click(function () {
                        $.fancybox.prev()
                    });
                    $('.new-next').click(function () {
                        $.fancybox.next()
                    })
                }

                $('.exit_fancy').click(function () {
                    $.fancybox.close();
                });
//                $('.top_fancy').each(function(){
//                    $(this).click(function(){
//                        $('.fancybox-overlay').animate({
//                            'scrollTop': 0
//                        },500);
//                        $('body').animate({
//                            'scrollTop': 0
//                        },500);
//                        $('html').animate({
//                            'scrollTop': 0
//                        },500);
//                    })
//                });
            })
        </script>


    </div>
    <div class="contact_box">
        <div id="in_yan"></div>
        <div class="container">
            <?php
            $contactForm = new ContactForm();
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'contact-form',
                'action' => array('site/contact'),
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnChange' => false,
                    'validateOnSubmit' => true,
                    'afterValidate' => "js: function(form, data, hasError) {
                            if ( !hasError) {
                                $.ajax({
                                    type: 'POST',
                                    url: $('#contact-form').attr('action'),
                                    data: $('#contact-form').serialize(),
                                    success: function(data_inner) {
                                        if ( data_inner==1 ) {

                                            $('#goSuccess a').click();
                                            $('#contact-form').trigger('reset');

                                        } else {
                                            alert('Хъюстон у нас проблемы!!!!');
                                        }
                                    }
                                });
                            }
                            return false;
                        }",
                ),
            )); ?>
            <?php echo $form->errorSummary($contactForm); ?>

            <input type="hidden" name="ContactForm[theme]" value="Нижняя форма"/>

            <?php echo $form->textField($contactForm, 'name', array('placeHolder' => 'Ваше имя', 'required' => 'required')); ?>
            <div class="dn"><?php echo $form->error($contactForm, 'name') ?></div>
            <?php echo $form->textField($contactForm, 'phone', array('placeHolder' => 'Телефон', 'required' => 'required')); ?>
            <div class="dn"><?php echo $form->error($contactForm, 'phone') ?></div>
            <?php echo $form->textArea($contactForm, 'commit', array('placeHolder' => 'Комментарий'), array()); ?>

            <?= CHtml::submitButton('', array()) ?>

            <?php $this->endWidget(); ?>
            <div id="top_footer_phone">
                <div class="city_selector">
                    <?php echo CHtml::dropDownList('city2', null, CHtml::listData($city, 'id', 'name'), array('empty' => 'Ваш город...', 'class' => 'styled')); ?>
                </div>
                <?php echo City::model()->findByPk(1)->phone ?>
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="container">
            <div id="copyright">
                © 2015. Ф.А.Р.Ш. Все права защищены
            </div>
            <a id="ex_img" arget="_blank" href="http://extralogic.ru/">
                <img src="/img/extra.png"/>
            </a>

            <div id="made_in_EXTRA_LOGIC">
                Создание сайта
            </div>
        </div>
    </div>
</div>
<!--Начало формы отправки письма-->
<?php
$modalForm = new ContactForm;
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'contactDialog',
    // additional javascript options for the dialog plugin
    'options' => array(
        'draggable' => false,
        'resizable' => false,
        'modal' => true,
        //'title'=>'Задать вопрос',
        'autoOpen' => false,
        'width' => '621',
        'height' => '570',
        'open' => 'js:function(){$(\'.ui-widget-overlay\').click(function(){
                $(\'#contactDialog\').dialog(\'close\');
            })}',
    ),
));
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'modal-form',
    'action' => array('site/contact'),
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnChange' => false,
        'validateOnSubmit' => true,
        'afterValidate' => "js: function(form, data, hasError) {
                if ( !hasError) {
                    $.ajax({
                        type: 'POST',
                        url: $('#modal-form').attr('action'),
                        data: $('#modal-form').serialize(),
                        success: function(data_inner) {
                            if ( data_inner==1 ) {
                                $('#contactDialog').dialog('close');
                                $('#goSuccess a').click();
                                $('#modal-form').trigger('reset');

                            } else {
                                alert('Хъюстон у нас проблемы!!!!');
                            }
                        }
                    });
                }
                return false;
            }"
    ),
)); ?>

<?php echo $form->errorSummary($modalForm); ?>
<div class="what_title"></div>
<input type="hidden" name="ContactForm[theme]" value="Всплывающее окно"/>
<?php echo $form->textField($modalForm, 'name', array('placeHolder' => 'Ваше имя', 'required' => 'required')); ?>
<div class="dn"><?php echo $form->error($modalForm, 'name') ?></div>
<?php echo $form->textField($modalForm, 'phone', array('placeHolder' => 'Телефон', 'required' => 'required')); ?>
<div class="dn"><?php echo $form->error($modalForm, 'phone') ?></div>
<?php echo $form->textArea($modalForm, 'commit', array('placeHolder' => 'Комментарий'), array()); ?>

<?= CHtml::submitButton('', array(
    'class' => 'what_button'
)) ?>

<?php $this->endWidget(); ?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
<!--Конец формы отправки письма-->