<?php

    $cs = Yii::app()->clientScript;
    $pt = Yii::app()->homeUrl;

//Yii::app()->clientScript->registerCssFile(CHtml::asset(Yii::app()->less->compileFile($pt.'css/main.less')));
//$cs->registerCssFile(CHtml::asset(Yii::app()->less->compileFile($pt.'css/main.less')));;
    $cs
        //->registerCssFile($pt.'css/bootstrap.min.css')
        //->registerCssFile($pt.'css/bootstrap-theme.min.css')
        //->registerCssFile($cs->getCoreScriptUrl().'/jui/css/base/jquery-ui.css')
        //->registerCssFile($pt.'css/non-responsive.css')
        //->registerCssFile($pt.'css/bootstrap-switch.min.css')
        //->registerCssFile($pt.'css/bootstrap-multiselect.css')
        //->registerCssFile($pt.'css/font-awesome.min.css')
        //->registerCssFile($pt.'css/awesome-bootstrap-checkbox.css')  
        //->registerCssFile($pt.'js/fancybox/jquery.fancybox.css')
        //->registerCssFile($pt.'css/bootstrap-editable.css') 
        //->registerCssFile($pt.'css/bootstrap-select.min.css')
        //->registerCssFile($pt.'css/bootstrap-datetimepicker.min.css')
        
        ->registerCssFile($pt.'css/reset.min.css')
        //->registerCssFile($pt.'css/anythingslider.css')
        ->registerCssFile($pt.'css/animate.css')
        //preLoader
        ->registerCssFile($pt.'css/preLoader.css')
        //custom_select
        ->registerCssFile($pt.'css/form.css')

        ->registerCssFile($pt.'css/common.css')
        //arcticmodal
        ->registerCssFile($pt.'css/jquery.arcticmodal-0.3.css')
        ->registerCssFile($pt.'css/modal_simple.css')
        ->registerCssFile($pt.'css/newFancyBoxStyle.css')
        ->registerCssFile($pt.'js/fancybox/jquery.fancybox.css')
        //->registerCssFile($pt.'css/cerulean.css')
        ->registerCssFile($pt.'css/main.css');
    
    $cs
        ->registerCoreScript('jquery',CClientScript::POS_END)
        ->registerCoreScript('jquery.ui',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/fancybox/jquery.fancybox.js',CClientScript::POS_END)
        //custom_select
        ->registerScriptFile($pt.'js/custom-form-elements.js',CClientScript::POS_END)
        ->registerCoreScript('cookie',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/holder.min.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/bootstrap-switch.min.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/bootstrap-multiselect.js',CClientScript::POS_END) 
        //->registerScriptFile($pt.'js/fancybox/jquery.fancybox.pack.js',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/jquery.scrollTo.min.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/jquery.tinycarousel.min.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/bootstrap-editable.min.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/bootstrap-select.min.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/moment.min.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/ru.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/bootstrap-datetimepicker.min.js',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/jquery.carouFredSel-6.1.0-packed.js',CClientScript::POS_END)

        
        
        ->registerScriptFile($pt.'js/common.js',CClientScript::POS_END)
            //arcticmodal
        ->registerScriptFile($pt.'js/jquery.arcticmodal-0.3.min.js',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/main.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/jquery.anythingslider.js',CClientScript::POS_END)
        //->registerScriptFile($pt.'js/less.min.js',CClientScript::POS_END)
        /*->registerScript('tooltip',
            "$('[data-toggle=\"tooltip\"]').tooltip();
            $('[data-toggle=\"popover\"]').tooltip()"
            ,CClientScript::POS_READY)*/;



    //$user = User::model()->findByPk(Yii::app()->user->id);
$config = Config::model()->findByPk(1);
?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!--<link rel="stylesheet/less" type="text/css" href="/css/main.less">-->
    <!--link rel="icon" type="image/png" href="/images/favicon.png" /-->
    
    <title><?=Yii::app()->name; ?></title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/png">
    <noindex><script async src="data:text/javascript;charset=utf-8;base64,ZnVuY3Rpb24gbG9hZHNjcmlwdChlLHQpe3ZhciBuPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoInNjcmlwdCIpO24uc3JjPSIvL2xwdHJhY2tlci5ydS9hcGkvIitlO24ub25yZWFkeXN0YXRlY2hhbmdlPXQ7bi5vbmxvYWQ9dDtkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKG4pO3JldHVybiAxfXZhciBpbml0X2xzdGF0cz1mdW5jdGlvbigpe2xzdGF0cy5zaXRlX2lkPTc2NjQ7bHN0YXRzLnJlZmVyZXIoKX07dmFyIGpxdWVyeV9sc3RhdHM9ZnVuY3Rpb24oKXtqUXN0YXQubm9Db25mbGljdCgpO2xvYWRzY3JpcHQoInN0YXRzX2F1dG8uanMiLGluaXRfbHN0YXRzKX07bG9hZHNjcmlwdCgianF1ZXJ5LTEuMTAuMi5taW4uanMiLGpxdWVyeV9sc3RhdHMp"></script></noindex>
</head>

<body>

<script>
    // прелоадер начало
    $(document).ready(function () {
        $('#loader-wrapper').animate({
            opacity: 0
        }, 2000, null, function () {
            $(this).hide();
        });
    });
    // прелоадер конец
</script>

<!--html-раметка прелоадера-->
<div id="loader-wrapper">
    <div id="loader"></div>
</div>

<section>
    <?= $content ?>
</section>

<div style="display: none" id="goSuccess">
    <a onclick="$('#goModal_ty').arcticmodal()"></a>
</div>
<div style="display: none;">
    <div class="box-modal bgr_go_contact" id="goModal_ty">
        <div class="close_modal box-modal_close arcticmodal-close modal_close"></div>
        <a  href="#" class="close_modal box-modal_close arcticmodal-close button_ok"></a>
    </div>
</div>

</body>
</html>