<?php

class Controller extends CController
{

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public $layout='//layouts/main';
	//public $menu=array();
	public $breadcrumbs=array();


    public function getNavigation()
    {
        $result = '';
        $i = 0;
        foreach ( $this->breadcrumbs as $url => $label ) {
            $i++;
            if ( $i==count($this->breadcrumbs) ) {
                $result .= $label;
            } else {
                $result .= BsHtml::link($label, $url).' / ';
            }
        }
        return $result;
    }
    
    public function getConfig()
    {
        return Config::model()->findByPk(1);
    }
        
}