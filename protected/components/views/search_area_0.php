<?php

    $request = Yii::app()->request;
    
    $region = Region::model()->findByPk($request->cookies['region']->value);
    
    $localities_0 = $region->locality_0_0;
    $cities_0 = array(
        0 => $region->name. ' ('.$region->count_0_0.')',
    );
    foreach ( $localities_0 as $locality ) {
        $cities_0[$locality->id] = $locality->name. ' ('.$locality->count_0_0.')';
    }
    
    $localities_1 = $region->locality_1_0;
    $cities_1 = array(
        0 => $region->name. ' ('.$region->count_1_0.')',
    );
    foreach ( $localities_1 as $locality ) {
        $cities_1[$locality->id] = $locality->name. ' ('.$locality->count_0_0.')';
    }
    
    $criteria = new CDbCriteria();
    $criteria->compare('region_id', $this->region);
    $criteria->addCondition('locality_id IS NULL');
    $criteria->order = '`name` ASC';
    $area = Area::model()->findAll($criteria);
    $areas = array();
    foreach ( $area as $a ) {
        $areas[$a->id] = $a->name;
    }
    
    $criteria = new CDbCriteria();
    $criteria->order = '`zIndex` ASC, `name` ASC';
    
    $type0 = Type0::model()->findAll($criteria);
    $types0 = array();
    foreach ( $type0 as $val ) {
        $types0[$val->id] = $val->name;
    }
    
    $objectLayout = ObjectLayout::model()->findAll($criteria);
    $objectLayouts = array();
    foreach ( $objectLayout as $val ) {
        $objectLayouts[$val->id] = $val->name;
    }
    
    $objectMaterial = ObjectMaterial::model()->findAll($criteria);
    $objectMaterials = array();
    foreach ( $objectMaterial as $val ) {
        $objectMaterials[$val->id] = $val->name;
    }
    
    $objectRepair0 = ObjectRepair0::model()->findAll($criteria);
    $objectRepairs0 = array();
    foreach ( $objectRepair0 as $val ) {
        $objectRepairs0[$val->id] = $val->name;
    }
    
    $objectBathroom = ObjectBathroom::model()->findAll($criteria);
    $objectBathrooms = array();
    foreach ( $objectBathroom as $val ) {
        $objectBathrooms[$val->id] = $val->name;
    }
    
?>
<script>
    $(document).ready(function(){
        $('.only_rubric_1').hide();
    });
</script>

<div class="row">
    <div class="col-xs-3">
        <div class="sf_el">
            <div class="row">
                <div class="col-xs-7">
                    <div class="myLabel">Я хочу</div>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default active changeRubric" rubric="0">
                            <input type="radio" autocomplete="off" checked> Купить
                        </label>
                        <label class="btn btn-default changeRubric" rubric="1">
                            <input type="radio" autocomplete="off"> Снять
                        </label>
                    </div>
                    <?= BsHtml::hiddenField('rubric', '0') ?>
                </div>
                <div class="col-xs-5 only_rubric_1">
                    <div class="myLabel">Тип аренды</div>
                    <div class="checkbox aw_check" style="margin-bottom: 0;">
                        <input type="checkbox" id="isDaily" name="isDaily">
                        <label for="isDaily">
                            Посуточно
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="sf_el">
            <div class="myLabel">Город</div>
            <div class="only_rubric_0">
                <?= BsHtml::dropDownList('city', '', $cities_0, array(
                    'id'=>'',
                    'class'=>'input_city',
                )) ?>
            </div>
            <div class="only_rubric_1">
                <?= BsHtml::dropDownList('city', '', $cities_1, array(
                    'id'=>'',
                    'class'=>'input_city',
                )) ?>
            </div>
        </div>
        <div class="sf_el sf_el_area">
            <?php /*if ( !$region->isRegion ): ?>
                <div class="myLabel">Район</div>
            <?php else: ?>
                <div class="myLabel">Район области</div>
            <?php endif;*/ ?>
            <div class="myLabel">Район</div>
            <?= BsHtml::dropDownList('area', '', $areas, array(
                'multiple'=>'multiple',
                'class'=>'multiselect',
            )) ?>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="sf_el">
            <div class="myLabel">Тип квартиры</div>
            <?= BsHtml::dropDownList('type_0', '', $types0, array(
                'multiple'=>'multiple',
                'class'=>'multiselect',
            )) ?>
        </div>
        <div class="sf_el">
            <div class="row">
                <div class="col-xs-8">
                    <div class="myLabel">Улица</div>
                    <?= BsHtml::textField('street_id', '', array(
                        'placeHolder'=>'Любая',
                    )) ?>
                </div>
                <div class="col-xs-4">
                    <div class="myLabel">Дом</div>
                    <?= BsHtml::textField('house', '', array(
                        'placeHolder'=>'№',
                    )) ?>
                </div>
            </div>
        </div>
        <div class="sf_el">
            <div class="myLabel">Комнат</div>
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default">
                    <input type="checkbox" autocomplete="off"> 1
                </label>
                <label class="btn btn-default">
                    <input type="checkbox" autocomplete="off"> 2
                </label>
                <label class="btn btn-default">
                    <input type="checkbox" autocomplete="off"> 3
                </label>
                <label class="btn btn-default">
                    <input type="checkbox" autocomplete="off"> 4
                </label>
                <label class="btn btn-default">
                    <input type="checkbox" autocomplete="off"> 5
                </label>
                <label class="btn btn-default">
                    <input type="checkbox" autocomplete="off"> 6+
                </label>
            </div>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="sf_el">
            <div class="myLabel">Общая площадь, кв. м.</div>
            <?= BsHtml::textField('space_total_from', '', array(
                'class'=>'w40p inline',
            )) ?>
            <div class="text-center inline w16p">&#8211;</div>
            <?= BsHtml::textField('space_total_to', '', array(
                'class'=>'w40p inline',
            )) ?>
        </div>
        <div class="sf_el">
            <div class="myLabel">Планировка</div>
            <?= BsHtml::dropDownList('layout_id', '', $objectLayouts, array(
                'multiple'=>'multiple',
                'class'=>'multiselect',
            )) ?>
        </div>
        <div class="sf_el">
            
            <div class="myLabel only_rubric_0">Цена, тыс. руб</div>
            <div class="myLabel only_rubric_1">Цена, руб/<span class="rentPeriod">месяц</span></div>
            <?= BsHtml::textField('price_from', '', array(
                'class'=>'w40p inline',
                'id'=>'',
            )) ?>
            <div class="text-center inline w16p">&#8211;</div>
            <?= BsHtml::textField('price_to', '', array(
                'class'=>'w40p inline',
                'id'=>'',
            )) ?>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="sf_el">
            <div class="myLabel">Дополнительно</div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="only_rubric_0">
                        <div class="checkbox aw_check">
                            <input type="checkbox" id="hasHypothec" name="hasHypothec">
                            <label for="hasHypothec">
                                Ипотека
                            </label>
                        </div>
                    </div>
                    <div class="only_rubric_1">
                        <div class="checkbox aw_check">
                            <input type="checkbox" id="hasPhone" name="hasPhone">
                            <label for="hasPhone">
                                Телефон
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="checkbox aw_check">
                        <input type="checkbox" id="hasPhoto" name="hasPhoto">
                        <label for="hasPhoto">
                            С фото
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="sf_el only_rubric_1">
            <div class="row">
                <div class="col-xs-6">
                    <div class="checkbox aw_check">
                        <input type="checkbox" id="hasFurniture" name="hasFurniture">
                        <label for="hasFurniture">
                            Мебель
                        </label>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="checkbox aw_check">
                        <input type="checkbox" id="hasIcebox" name="hasIcebox">
                        <label for="hasIcebox">
                            Холодильник
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <!--div class="sf_el text-right">
            <?= BsHtml::link('Подробный поиск', '#', array('onclick'=>'return false;')) ?>
        </div-->
    </div>
</div>

<?= BsHtml::submitButton('Найти', array(
    'color'=>BsHtml::BUTTON_COLOR_SUCCESS,
    //'size'=>BsHtml::BUTTON_SIZE_LARGE,
    'block'=>true,
    'icon'=>BsHtml::GLYPHICON_SEARCH,
    'url'=>'#',
    //'onclick'=>'return false;',
    'id'=>'sa_submit',
)) ?>

<div id="additionalSearchArea" style="display: none;">
    <hr />
    <div class="row">
        <div class="col-xs-3">
            <div class="sf_el">
                <div class="myLabel">Жилая площадь, кв. м.</div>
                <?= BsHtml::textField('space_living_from', '', array(
                    'class'=>'w40p inline',
                )) ?>
                <div class="text-center inline w16p">&#8211;</div>
                <?= BsHtml::textField('space_living_to', '', array(
                    'class'=>'w40p inline',
                )) ?>
            </div>
            <div class="sf_el">
                <div class="myLabel">Площадь кухни, кв. м.</div>
                <?= BsHtml::textField('space_kitchen_from', '', array(
                    'class'=>'w40p inline',
                )) ?>
                <div class="text-center inline w16p">&#8211;</div>
                <?= BsHtml::textField('space_kitchen_to', '', array(
                    'class'=>'w40p inline',
                )) ?>
            </div>
            <div class="sf_el">
                <div class="myLabel">Материал дома</div>
                <?= BsHtml::dropDownList('material_id', '', $objectMaterials, array(
                    'multiple'=>'multiple',
                    'class'=>'multiselect',
                )) ?>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="sf_el">
                <div class="myLabel">Ремонт</div>
                <?= BsHtml::dropDownList('repair_0_id', '', $objectRepairs0, array(
                    'multiple'=>'multiple',
                    'class'=>'multiselect',
                )) ?>
            </div>
            <div class="sf_el">
                <div class="myLabel">Санузел</div>
                <?= BsHtml::dropDownList('bathroom_id', '', $objectBathrooms, array(
                    'multiple'=>'multiple',
                    'class'=>'multiselect',
                )) ?>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="sf_el">
                <div class="myLabel">Этаж</div>
                <?= BsHtml::textField('floor_from', '', array(
                    'class'=>'w40p inline',
                )) ?>
                <div class="text-center inline w16p">&#8211;</div>
                <?= BsHtml::textField('floor_to', '', array(
                    'class'=>'w40p inline',
                )) ?>
            </div>
            <div class="sf_el">
                <div class="myLabel">Этажность</div>
                <?= BsHtml::textField('countFloors_from', '', array(
                    'class'=>'w40p inline',
                )) ?>
                <div class="text-center inline w16p">&#8211;</div>
                <?= BsHtml::textField('countFloors_to', '', array(
                    'class'=>'w40p inline',
                )) ?>
            </div>
            <div class="sf_el">
                <div class="myLabel">Ориентир</div>
                <?= BsHtml::textField('landmark', '', array(
                    'placeHolder'=>'Ориентир',
                )) ?>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="sf_el only_rubric_0">
                <div class="myLabel">Дополнительно</div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="checkbox aw_check">
                            <input type="checkbox" id="hasPhone2" name="hasPhone2">
                            <label for="hasPhone2">
                                Телефон
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="checkbox aw_check">
                            <input type="checkbox" id="hasExchange" name="hasExchange">
                            <label for="hasExchange">
                                Обмен
                            </label>
                        </div>
                    </div>
                </div>
                <div class="checkbox aw_check">
                    <input type="checkbox" id="hasNetSale" name="hasNetSale">
                    <label for="hasNetSale">
                        Чистая продажа
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="add_search_area">
    <?= BsHtml::link('Подробный поиск', '#', array()) ?>
</div>