<?php

Class AdminController extends Controller
{
    public $layout='/layouts/main';
    //public $header='';
    //public $menu=array();
    public $breadcrumbs=array();

    public function filters()
    {
        return array(
            'accessControl',
        );
    }


    public function accessRules()
    {
        return array(
            array('allow',
                'controllers'=>array('admin/default'),
                'actions'=>array('login'),
                'users'=>array('*'),
            ),
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
                'deniedCallback' => function() { Yii::app()->controller->redirect(array('/admin/default/login')); },
            ),
        );
    }

    public function get_menu_left()
    {
        return array(
            array(
                'label'=>'Конфигурация',
                'url'=>array('/admin/config/index'),
                'visible' => !Yii::app()->user->isGuest,
            ),
            /*array(
                'label'=>'Портфолио старое',
                'url'=>array('/admin/portfolio/index'),
                'visible' => !Yii::app()->user->isGuest,
            ),*/
            array(
                'label'=>'Портфолио',
                'url'=>array('/admin/project/index'),
                'visible' => !Yii::app()->user->isGuest,
            ),
            array(
                'label'=>'Заявки',
                'url'=>array('/admin/contactForm/index'),
                'visible' => !Yii::app()->user->isGuest,
            ),
            array(
                'label'=>'Мета теги',
                'url'=>array('/admin/metaTags/index'),
                'visible' => !Yii::app()->user->isGuest,
            ),
            array(
                'label'=>'Список телефонов и городов',
                'url'=>array('/admin/city/index'),
                'visible' => !Yii::app()->user->isGuest,
            ),
        );
    }

    public function get_menu_right()
    {
        return array(
            array(
                'label' => '<i class="fa fa-sign-in"></i> '.'Вход',
                'url' => array('/admin/default/login'),
                'visible' => Yii::app()->user->isGuest,
            ),
            array(
                'label' => 'Выход',
                'icon' => BsHtml::GLYPHICON_LOG_OUT,
                'url' => array('/admin/default/logout'),
                'visible' => !Yii::app()->user->isGuest,
            ),
        );
    }
    
    
}