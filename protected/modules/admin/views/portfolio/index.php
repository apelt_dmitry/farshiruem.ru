<?php echo BsHtml::pageHeader('Портфолио') ?>
<script>
    $(document).ready(function(){
        
        $( ".sortable" ).sortable({
            stop: function( event, ui ) {
                //console.log(ui.item.index(),ui.item.find('img').attr('data-id'));
                    //console.log(ui.item.parent().parent().prev().html());
                    //console.log(ui.item.find('img').attr('id_portfolio'));
                    //id_prof = $("td").html();
                    //var data_id = [];
                    //$("td"+ui.item.find('img').attr('data-id')).each(function(indx){
                        //data_id.push($(this).attr('data-id'));
                    //});
                    //console.log(data_id);
                    var id_portfolio = ui.item.find('img').attr('id_portfolio');
                    var position = 0;
                    var id_slide = [];
                    
                    
                    
                    $('img[id_portfolio="'+id_portfolio+'"]').each(function(){
                        id_slide[position] = $(this).attr('data-id');
                        position++;
                    })
                 
                    console.log(id_slide);
                    
                $.ajax({
                    url: '/admin/portfolio/_ajax?action=change_position',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        id_slide: id_slide,
                        
                    },
                    success: function(data) {
                        
                    }
                });
            }
        });
    });
</script>
<style>
    ul.sortable{
        list-style: none;
        padding: 0;
    }
    ul.sortable li{
        float: left;
        margin-right: 10px;
        cursor: move;
    }
</style>

<?= BsHtml::linkButton('Добавить портфолио', array(
    'icon' => BsHtml::GLYPHICON_PLUS,
    'color' => BsHtml::BUTTON_COLOR_SUCCESS,
    'url' => array('/admin/portfolio/create'),
    //'target'=>'_blank',
    //'block' => true,
    'style'=>'float: right;margin-right:10px;',
)); ?>

<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>null,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
       array(
            'name' => 'id',
        ),
        array(
            'header' => 'Изображения',
            'type'=>'raw',
            'value' =>'$data->_preview',
        ),
        array(
            'name' => 'title',
        ),
        array(
            'name' => 'date',
        ),
        array(
            'name' => 'name',
        ),
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            'template' => '{update}{delete}',
        ),
    ),
)); ?>