<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'menu-form',
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnChange'=>false,
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
)); ?>
    <?php echo $form->errorSummary($model); ?>
   
    <?= $form->textFieldControlGroup($model,'title',array(
        'maxlength'=>32,
    )); ?>
    <?= $form->textFieldControlGroup($model,'date',array(
        'maxlength'=>16,
    )); ?>
    <?= $form->textFieldControlGroup($model,'name',array(
        'maxlength'=>32,
    )); ?>
    <?= $form->fileFieldControlGroup($model,'image[]',array(
        'help'=>'До 12 изображений любого размера.'.( (!$model->isNewRecord)?' Выбор новых изображений удалит текущие (кол-во: '.count($model->portfolioSlide).').':'' ),
        'multiple'=>'multiple',
    )); ?>

 
        <?php  if( $scenario == 'update' ){ 
       
           
            $allSlide = $model->portfolioSlide;
            foreach( $allSlide as  $slide ){ ?>
                <div class="row">
                    <div class="container">
                        <div class="col-lg-4">
                            <img src="/uploads/portfolio/preview/<?= $slide->preview; ?>"/>
                        </div>
                        <div class="col-lg-4">
                            <p>Позиция: <?= $slide->position ?></p>
                        </div>
                        <div class="col-lg-4" >
                            <?= BsHtml::formActions(array(
                                    BsHtml::button('Удалить', array(
                                        'color' => BsHtml::BUTTON_COLOR_WARNING,
                                        'icon' => BsHtml::GLYPHICON_FIRE,
                                        'id_slide' => $slide->id,
                                        )
                                    ),
                            )); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php }?>
    

<script>
    $(document).ready(function(){
        $('button[id_slide]').click(function(){
            var slide = $(this).attr('id_slide');
                $.ajax({
                     url: '/admin/portfolio/_ajax?action=id_slide',
                     type: 'POST',
                     dataType: 'json',
                     data: {
                         id: slide
                     },
                     success: function(data) {
                        if(data == 'ok'){
                            $('button[id_slide="'+slide+'"]').parent().parent().parent().parent().css('display','none');
                        }
                        if(data == 'not_ok'){
                            alert('При удалении произошла ошибка, повторите позднее.');
                        }    
                    }
                });
        });
    });
</script>
  

    <?= BsHtml::formActions(array(
        BsHtml::submitButton('Готово', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
    ), array('class'=>'form-actions')); ?>
<?php $this->endWidget(); ?>



