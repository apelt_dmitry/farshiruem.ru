<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    //'enableAjaxValidation' => true,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnChange'=>true,
        'validateOnSubmit'=>true,
    ),
    'id' => 'user_form_horizontal',
    'htmlOptions' => array(
        //'class' => 'bs-example'
    ),

));
?>
    <fieldset>
        <legend>Авторизация</legend>

        <?=
        $form->textFieldControlGroup($model, 'login', array(
            'prepend' => BsHtml::icon(BsHtml::GLYPHICON_USER),
        ));
        ?>

        <?=
        $form->passwordFieldControlGroup($model, 'password', array(
            'prepend' => BsHtml::icon(BsHtml::GLYPHICON_LOCK),
        ));
        ?>

    </fieldset>
<?php
echo BsHtml::formActions(array(
    BsHtml::submitButton('Войти', array(
        'color' => BsHtml::BUTTON_COLOR_PRIMARY
    ))
));
?>

<?php
$this->endWidget();
?>