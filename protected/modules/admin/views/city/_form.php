<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'menu-form',
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnChange'=>false,
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name'); ?>

    <?php echo $form->textFieldControlGroup($model,'phone'); ?>



    <?= BsHtml::formActions(array(
        BsHtml::linkButton('Отмена', array(
            'color' => BsHtml::BUTTON_COLOR_DANGER,
            'icon' => BsHtml::GLYPHICON_REFRESH,
            'url' => '/admin/city/index'
        )),
        BsHtml::submitButton('Применить', array(
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'icon' => BsHtml::GLYPHICON_FLOPPY_SAVE,
            'name'=>'ok',
        )),
        BsHtml::submitButton('Сохранить', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon'=> BsHtml::GLYPHICON_FLOPPY_SAVED,
            'name'=>'ok2',
        )),
    ), array('class'=>'form-actions')); ?>

<?php $this->endWidget(); ?>
