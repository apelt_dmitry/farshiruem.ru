<?php
    $cs = Yii::app()->clientScript;
    $pt = Yii::app()->homeUrl;

    $cs
        ->registerCssFile($pt.'css/bootstrap.min.css')
        //->registerCssFile($pt.'css/bootstrap-theme.min.css')
        //->registerCssFile($cs->getCoreScriptUrl().'/jui/css/base/jquery-ui.css')
        ->registerCssFile($pt.'css/font-awesome.min.css')
        ->registerCssFile($pt.'css/cerulean.css')
        ->registerCssFile($pt.'css/common.css')
        ->registerCssFile($pt.'css/admin.css');    
    
    $cs
        ->registerCoreScript('jquery',CClientScript::POS_END)
        ->registerCoreScript('jquery.ui',CClientScript::POS_END)
        ->registerCoreScript('cookie',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/holder.min.js',CClientScript::POS_END)
            
        ->registerScriptFile($pt.'js/common.js',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/admin.js',CClientScript::POS_END)

        /*->registerScript('tooltip',
            "$('[data-toggle=\"tooltip\"]').tooltip();
            $('[data-toggle=\"popover\"]').tooltip()"
            ,CClientScript::POS_READY)*/;

    //$user = User::model()->findByPk(Yii::app()->user->id);
    //$config = $this->config;
?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <link rel="icon" type="image/png" href="/images/favicon.png" />

    <title><?= CHtml::encode($this->pageTitle); ?></title>

</head>

<body>
    
    <div class="container-fluid">
        
        <div class="row">
            
            <div id="header">
                <?php
                    $this->widget('bootstrap.widgets.BsNavbar', array(
                        'brandLabel' => BsHtml::icon(BsHtml::GLYPHICON_HOME),
                        'brandUrl' => Yii::app()->homeUrl,
                        'brandOptions'=>array(
                            'title'=>'Вернуться на сайт',
                            'data-toggle'=>'tooltip',
                            'data-placement'=>'bottom',
                            'target'=> '_blank'
                        ),
                        'items' => array(
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => $this->_menu_left,
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => $this->_menu_right,
                                'htmlOptions' => array(
                                    'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT,
                                    'style' => 'margin-right: 10px;',
                                ),
                            ),
                        ),
                    ));
                ?>
            </div>

            <div id="content">
                <?= $content ?>
            </div>

            <div id="footer" class="panel panel-footer panel-default text-muted">
                <div class="row">
                    <div class="col-lg-6">
                        <div id="timeInfo" class="text-left"><small>Время сервера: <?= date('d.m.Y, H:i') ?>.</small></div>
                    </div>
                    <div class="col-lg-6">
                        <div id="logInfo" class="text-right"><small>Страница сгенерирована за: <?=round(Yii::getLogger()->executionTime, 3).' сек' ?>.</small></div>
                    </div>
                </div>
            </div>

        </div>
        
    </div>
    
</body>
</html>