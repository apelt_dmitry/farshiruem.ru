<?php
    $this->breadcrumbs[] = 'Изменение проекта №'.$model->id;
?>    
    
<?php $this->beginWidget('bootstrap.widgets.BsPanel', array(
    'title' => $this->navigation,
)); ?>

    <div class="buttons_for_panel">
        <?= BsHtml::linkButton('Вернуться к списку записей', array(
            'icon' => BsHtml::GLYPHICON_BACKWARD,
            'color' => BsHtml::BUTTON_COLOR_DEFAULT,
            'url' => array('index'),
        )); ?>        
        <?= BsHtml::linkButton('Удалить запись', array(
            'icon' => BsHtml::GLYPHICON_TRASH,
            'color' => BsHtml::BUTTON_COLOR_DANGER,
            'url' => array('delete', 'id'=>$model->id),
            'onclick'=>'if ( !confirm(\'Действительно удалить?\') ) return false;',
        )); ?>
    </div>

    <?php $this->renderPartial('_form', array('model'=>$model)); ?>

<?php $this->endWidget(); ?>