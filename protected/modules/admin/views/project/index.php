<?php
  //  $drop_list = array('' => 'Все', 1 => 'Landing page', 2 => 'Web разработка', 3 => 'Разработка ПО');
  //  $this->breadcrumbs[] = 'Список проектор';
?>    

<?php $this->beginWidget('bootstrap.widgets.BsPanel', array(
    'title' => $this->navigation,
)); ?>

    <div class="buttons_for_panel">
        <?= BsHtml::linkButton('Добавить запись', array(
            'icon' => BsHtml::GLYPHICON_PLUS,
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'url' => array('create'),
        )); ?>
    </div>

    <?php $this->widget('bootstrap.widgets.BsGridView',array(
        'id'=>'project-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
        'template' => '{summary}{items}{pager}',
        'pager'=>array(
            'class' => 'bootstrap.widgets.BsPager',
            'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
        ),
        'nullDisplay'=>'-',
        'selectableRows'=>0,
        //'enableSorting'=>false,
        //'beforeAjaxUpdate'=>'function(){beforeUpdateGrid_project();}',
        //'afterAjaxUpdate'=>'function(){updateGrid_project();}',
        'columns'=>array(
            //array(
            //    'class'=>'CCheckBoxColumn',
            //    //'checkBoxHtmlOptions' => array('class' => 'classname'),
            //),
            
            //array(
            //    'name'=>'date_create',
            //    'value'=>'Yii::app()->dateFormatter->format(\'dd.MM.yy, HH:mm\', $data->date_create)',
            //    'filter'=>false,
            //    //'sortable'=>false,
            //),
            
            //array(
            //    'name'=>'role',
            //    'value'=>'$data->_role',
            //    'filter'=>BsHtml::activeDropDownList($model, 'role', array(
            //        ''=>'-',
            //        '0'=>'',
            //        '1'=>'',
            //        '2'=>'',
            //    )),
            //),

            //array(
            //    //'header'=>'Действия',
            //    'class'=>'bootstrap.widgets.BsButtonColumn',
            //    //'template'=>'{view} {update} {delete}',
            //),

        [
            'name' => 'image_view',
            'filter' => false,
            'type' => 'raw',
            'value' => '$data->imageView'
        ],

		[
            'name' => 'name_ru',
            'filter' => false,
        ],

        /*[
            'name' => 'link',
            'filter' => false,
        ],*/

        [
            'name' => 'date',
            'filter' => false,
        ],

        [
            'name' => 'sub_name',
            'filter' => false,
        ],

        [
            'name' => 'sort',
            'filter' => false,
        ],

        [
            'name' => 'description_detail_ru',
            'filter' => false,
        ],

        /*[
            'name' => 'type',
            'filter' => BsHtml::activeDropDownList($model, 'type', $drop_list),
            'value' => '$data->typeSite',
            'headerHtmlOptions' => [
                'width' => 150
            ]
        ],*/

            array(
                'header'=>'',
                'value'=>''
                . 'BsHtml::linkButton("", array("url"=>array("delete", "id"=>$data->id), "size"=>BsHtml::BUTTON_SIZE_SMALL, "data-toggle"=>"tooltip", "title"=>"Удалить", "icon"=>BsHtml::GLYPHICON_TRASH, "color"=>BsHtml::BUTTON_COLOR_DEFAULT, "onclick"=>"if ( !confirm(\'Действительно удалить?\') ) return false;"))." ".'
                . 'BsHtml::linkButton("", array("url"=>array("update", "id"=>$data->id), "size"=>BsHtml::BUTTON_SIZE_SMALL, "data-toggle"=>"tooltip", "title"=>"Изменить", "icon"=>BsHtml::GLYPHICON_PENCIL, "color"=>BsHtml::BUTTON_COLOR_WARNING))',
                'htmlOptions'=>array(
                    'class'=>'myActions',
                ),
                'type'=>'raw',
            ),
        ),
    )); ?>    
    
<?php $this->endWidget(); ?>


