<?php if ( Yii::app()->user->getFlash('success') ): ?>
    <?= BsHtml::alert(Bshtml::ALERT_COLOR_SUCCESS, 'Информация обновлена.') ?>
<?php endif; ?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'project-form',
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnChange'=>true,
        'validateOnSubmit'=>true,
    ),
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data',
    ),
)); ?>

    <?= $form->errorSummary($model); ?>    
    <fieldset>
    
        <?= $form->textFieldControlGroup($model,'name_ru',array('maxlength'=>255)); ?>
        <?= $form->textFieldControlGroup($model,'sort',array('maxlength'=>255)); ?>
        <?= $form->textFieldControlGroup($model,'date',array('maxlength'=>32)); ?>
        <?= $form->textFieldControlGroup($model,'sub_name',array('maxlength'=>32)); ?>
        <?= $form->textAreaControlGroup($model,'description_detail_ru',array('rows'=>6)); ?>

        <?= $form->fileFieldControlGroup($model,'_image_view',array(
            'help'=> ($model->isNewRecord) ? 'Допустимые форматы jpg, gif, png':'Загрузка нового изображения удалит текущее.',
            'multiple'=>'multiple',
        )); ?>

        <?= $form->fileFieldControlGroup($model,'_image',array(
            'help'=> ($model->isNewRecord) ? 'Допустимые форматы jpg, gif, png':'Загрузка нового изображения удалит текущее.',
            'multiple'=>'multiple',
        )); ?>
        
    </fieldset>

    <?= BsHtml::formActions(array(
        BsHtml::submitButton('Сохранить', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
    ), array('class'=>'form-actions')); ?>    

<?php $this->endWidget(); ?>
