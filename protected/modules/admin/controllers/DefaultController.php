<?php

Class DefaultController extends AdminController
{
    

    public function actionLogin()
    {
        //$this->layout = 'login';
        
        $model = new User('login');

        if ( isset($_POST['ajax']) && $_POST['ajax']==='login-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if ( isset($_POST['User']) ) {
            $model->attributes = $_POST['User'];
            if ( $model->validate() && $model->login() ) {
                $this->redirect(array('index'));
            }
        }
        $this->render('login', array(
            'model'=>$model,
        ));
    }
    
    /*public function actionCreateUser(){
        $model = new User();
        $model->login = 'admin';
        $model->password = CPasswordHelper::hashPassword('257nP372');
        $model->save(false);
    }*/

    public function actionIndex()
    {

        $this->redirect(array('/admin/contactForm/index'));
        $this->render('index', array(
            //'model'=>$model,
        ));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}