<?php

Class ConfigController extends AdminController
{

    public function actionIndex()
    {
        $model = $this->config;

        if( isset($_POST['Config']) ) {
            $model->attributes = $_POST['Config'];
            
            if ( $model->save() ) {
                Yii::app()->user->setFlash('success', true);
            }

            if( !empty($_POST['oldPassword']) && !empty($_POST['newPassword']) ){

                $change_password = User::model()->findByPk(1);
                if( CPasswordHelper::verifyPassword($_POST['oldPassword'], $change_password->password) ){
                    $change_password->password = CPasswordHelper::hashPassword($_POST['newPassword']);
                    $change_password->save(false);
                }
                else{
                    Yii::app()->user->setFlash('fail', true);
                }

            }
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }
    
}