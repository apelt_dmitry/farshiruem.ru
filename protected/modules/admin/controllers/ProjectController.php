<?php

Class ProjectController extends AdminController
{
    

    public function init() {
        parent::init();
        
        $this->breadcrumbs = array(
            '/admin/project/index'=>'Управление портфолио',
        );
    }

    
    public function actionIndex()
    {
        $model = new Project('search');
        $model->unsetAttributes();
        
        if ( isset($_GET['Project']) ) {
            $model->attributes = $_GET['Project'];
        }
        
        $this->render('index', array(
            'model'=>$model,
        ));
    }

    
    public function actionCreate()
    {
        $model = new Project('create');
        
        if ( isset($_POST['Project']) ) {
            $model->attributes = $_POST['Project'];

            $model->_image_view = CUploadedFile::getInstance($model,'_image_view');
            $model->_image = CUploadedFile::getInstance($model,'_image');
            $file = $model->_image_view;
            if($model->validate()){
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/project/view/'.$imageName);
                    $image->resize(235, 235);
                    $image->save('./uploads/project/view/preview/'.$imageName);
                    $model->image_view = $imageName;
                }
                $file = $model->_image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/project/site/'.$imageName);
                    $image->resize(235, 235);
                    $image->save('./uploads/project/site/preview/'.$imageName);
                    $model->image = $imageName;
                }
                if ( $model->save(false) ) {
                    Yii::app()->user->setFlash('success', true);
                }
            }

        }
        
        $this->render('create', array(
            'model'=>$model,
        ));
    }
    
    
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $model->scenario = 'update';
        
        if ( isset($_POST['Project']) ) {
            $model->attributes = $_POST['Project'];

            $model->_image_view = CUploadedFile::getInstance($model,'_image_view');
            $model->_image = CUploadedFile::getInstance($model,'_image');
            $file = $model->_image_view;
            if($model->validate()){
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/project/view/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/project/view/preview/'.$imageName);
                    $model->image_view = $imageName;
                }
                $file = $model->_image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/project/site/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/project/site/preview/'.$imageName);
                    $model->image = $imageName;
                }
                if ( $model->save(false) ) {
                    Yii::app()->user->setFlash('success', true);
                }
            }

        }
        
        $this->render('update', array(
            'model'=>$model,
        ));
    }
    
    
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
    }
    
    
    public function loadModel($id)
    {
        $model = Project::model()->findByPk($id);
        if ( !$model ) {
            throw new CHttpException(404, 'Запись не найдена.');
        }
        return $model;
    }
    
    
    
}