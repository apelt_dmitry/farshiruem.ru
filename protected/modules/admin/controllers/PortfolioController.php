<?php

class PortfolioController extends AdminController
{

    
    public function actionCreate()
    {
        $model = new Portfolio();
        
        if ( isset($_POST['Portfolio']) ) {
            $model->attributes = $_POST['Portfolio'];
            $model->image=CUploadedFile::getInstances($model,'image');
            if($model->save()){
                foreach($model->image as $key=>$file) {
                    if ( $file->name!='' ) {
                        $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                        $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                        $image = Yii::app()->image->load($file->tempName);
                        $image->save('./uploads/portfolio/'.$imageName);
                        $image->resize(235, 235);
                        $image->save('./uploads/portfolio/preview/'.$imageName);
                        $portfolioSlide = new PortfolioSlide();
                        $portfolioSlide->id_portfolio= $model->id;
                        $portfolioSlide->preview= $imageName;
                        $portfolioSlide->position=$key;
                        $portfolioSlide->save();
                    }
                }
                $this->redirect(array('index'));
            };
        }
        $this->render('create', array(
            'model'=>$model,
        ));
    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

                if ( isset($_POST['Portfolio']) ) {
            $model->attributes = $_POST['Portfolio'];
            $model->image=CUploadedFile::getInstances($model,'image');
            
            if($model->save()){
               
                foreach($model->image as $file) {
                    
                    if ( $file->name!='' ) {
                        $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                        $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                        $image = Yii::app()->image->load($file->tempName);
                        $image->save('./uploads/portfolio/'.$imageName);
                        $image->resize(235, 235);
                        $image->save('./uploads/portfolio/preview/'.$imageName);
                        $portfolioSlide = new PortfolioSlide();
                        $portfolioSlide->id_portfolio= $model->id;
                        $portfolioSlide->preview= $imageName;
                        $portfolioSlide->save();
                    }
                }
                $this->redirect(array('index'));
            };
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Portfolio('search');
        $model->unsetAttributes();
        if ( isset($_GET['Portfolio']) ) {
            $model->attributes=$_GET['Portfolio'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Portfolio::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function action_ajax()
    {
        $request = Yii::app()->request;
        
        if ( $request->getParam('action')=='change_position' ) {
            
            foreach ( $request->getParam('id_slide') as $key => $val ){
                PortfolioSlide::model()->updateByPk($val,array(
                    'position'=>$key
                ));
            }
            echo 'ok';
        }
        
        if ( $request->getParam('action')=='id_slide' ) {
            
        
            $del =$request->getParam('id');
            if(PortfolioSlide::model()->deleteByPk($del)){
                echo  json_encode('ok');
               
            }else{
                echo json_encode('not_ok');
            }
            
            
            
            
        }
        
        
    }
    
}
