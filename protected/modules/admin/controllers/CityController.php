<?php

class CityController extends AdminController
{


    public function actionCreate()
    {
        $model = new City();

        if ( isset($_POST['City']) && isset($_POST['ok2'] )) {
            $model->attributes = $_POST['City'];
            if( $model->save() ) {
                $this->redirect(array('index'));
            }
        } elseif (isset($_POST['City']) && isset($_POST['ok'])) {
            $model->attributes = $_POST['City'];
            if( $model->save() ) {
                //$this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'model'=>$model,
        ));
    }


    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if ( isset($_POST['City']) && isset($_POST['ok2'])) {
            $model->attributes = $_POST['City'];
            if( $model->save() ) {
                $this->redirect(array('index'));
            }
        } elseif (isset($_POST['City']) && isset($_POST['ok'])) {
        $model->attributes = $_POST['City'];
            if( $model->save() ) {
              //  $this->redirect(array('index'));
            }
    }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {
        
        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new City('search');
        $model->unsetAttributes();
        if ( isset($_GET['City']) ) {
            $model->attributes=$_GET['City'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=City::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


}
