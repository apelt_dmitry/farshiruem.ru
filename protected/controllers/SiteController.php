<?php

class SiteController extends Controller
{
    public function actionIndex()
    {

        $meta = MetaTags::model()->findByPk(1);
        $portfolios = Portfolio::model()->findAll();
        $city = City::model()->findAll();
//        if ( isset($_POST['ContactForm']) ) {
//                $model = new ContactForm();
//
//                $model->attributes = $_POST['ContactForm'];
//                if ( $model->validate() ) {
//                    $model->goContact();
//                    Yii::app()->user->setFlash('success', true);
//                    $model->date = time();
//                    $model->ip = Yii::app()->request->userHostAddress;
//                    $model->save(false);
//                }
//                //Yii::app()->end();
//        }

        $criteria = new CDbCriteria();
        $criteria->order = '`sort` ASC';
        $project = Project::model()->findAll($criteria);

        $this->render('index', array(
                'meta' => $meta,
                'portfolios' => $portfolios,
                'city' => $city,
                'project'=> $project
            ));
    }
    
    public function actionContact()
        {
            $model = new ContactForm();

            if ( isset($_POST['ajax']) && $_POST['ajax']==='contact-form' ) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            $model->attributes = $_POST['ContactForm'];
            if ( $model->validate() ) {
                Yii::app()->user->setFlash('success', true);
                $model->date = time();
                $model->ip = Yii::app()->request->userHostAddress;
                $model->save(false);
                $model->goContact();
                echo '1';
            } else {
                echo '0';
                print_r($model->errors);
            }
            //$this->redirect(array('site/index'));
        }
    
    public function actionError()
    {
        if( $error=Yii::app()->errorHandler->error ) {
            if ( Yii::app()->request->isAjaxRequest ) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
    public function actionApp()
    {
        require_once 'app.php';
    }
    
    
    public function action_ajax()
    {
        $request = Yii::app()->request;
        
        if ( $request->getParam('action')=='id' ) {
            $id = $request->getParam('id');
            $phone = City::model()->findByPk($id)->phone;
            echo json_encode($phone);
        }
    }
}