-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `meat` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `meat`;

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `phone` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `city` (`id`, `name`, `phone`) VALUES
(2,	'Омск',	'+7(381)297-21-56'),
(6,	'Новосибирск',	'+7(383)207-91-36');

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `vk` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password_mail_order` varchar(128) NOT NULL,
  `manager_order_mail` varchar(128) NOT NULL,
  `mail_order` varchar(128) NOT NULL,
  `yandex` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `config` (`vk`, `facebook`, `instagram`, `id`, `password_mail_order`, `manager_order_mail`, `mail_order`, `yandex`) VALUES
('http://vk.com/farshiruem',	'https://www.facebook.com/pages/ФАРШ/652243681579368',	'',	1,	'3W84R3GWxty',	'mitridat115@yandex.ru',	'i525643@gmail.com',	'<!-- -->');

DROP TABLE IF EXISTS `contactForm`;
CREATE TABLE `contactForm` (
  `theme` varchar(64) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `commit` varchar(255) DEFAULT NULL,
  `date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `contactForm` (`theme`, `id`, `ip`, `name`, `phone`, `commit`, `date`) VALUES
(NULL,	1,	'127.0.0.1',	'qwe',	'1234',	'',	1439466480),
(NULL,	2,	'127.0.0.1',	'qwe',	'1234',	'',	1439466697),
(NULL,	3,	'127.0.0.1',	'qwe',	'1234',	'',	1439466771),
(NULL,	4,	'127.0.0.1',	'qwe',	'1234',	'',	1439466817),
(NULL,	5,	'127.0.0.1',	'qwe',	'1234',	'',	1439467273),
(NULL,	6,	'127.0.0.1',	'qwe',	'1234',	'',	1439467337),
(NULL,	7,	'127.0.0.1',	'qwe',	'1234',	'',	1439467516),
(NULL,	8,	'127.0.0.1',	'qwe',	'1234',	'',	1439467671),
(NULL,	9,	'127.0.0.1',	'qwe',	'1234',	'waw',	1439467809),
(NULL,	10,	'127.0.0.1',	'qwe',	'1234',	'aw',	1439467890),
(NULL,	11,	'127.0.0.1',	'qwe',	'1234',	'awd',	1439468114),
(NULL,	12,	'127.0.0.1',	'qwe',	'1234',	'awd',	1439468116),
(NULL,	13,	'127.0.0.1',	'sadsa',	'1234',	'awd',	1439468133),
(NULL,	14,	'127.0.0.1',	'qwe',	'123',	'43',	1439468732),
(NULL,	15,	'127.0.0.1',	'qwe',	'1234',	'awd',	1439469091),
(NULL,	16,	'127.0.0.1',	'sadsa',	'1234',	'aw',	1439469148),
(NULL,	17,	'127.0.0.1',	'sadsa',	'1234',	'',	1439469191),
(NULL,	18,	'127.0.0.1',	'qwe',	'1234',	'sdf',	1439469298),
(NULL,	19,	'127.0.0.1',	'sadsa',	'1234',	'',	1439469432),
(NULL,	20,	'127.0.0.1',	'qwe',	'1234',	'qwe',	1439529524),
(NULL,	21,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580650),
(NULL,	22,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580653),
(NULL,	23,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580655),
(NULL,	24,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580657),
(NULL,	25,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580659),
(NULL,	26,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580661),
(NULL,	27,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580663),
(NULL,	28,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580665),
(NULL,	29,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580667),
(NULL,	30,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580669),
(NULL,	31,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580671),
(NULL,	32,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580673),
(NULL,	33,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580675),
(NULL,	34,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580677),
(NULL,	35,	'192.168.0.41',	'ыв',	'ыва',	'ыава',	1440580679),
(NULL,	36,	'192.168.0.41',	'ываыв',	'аываыва',	'аыва',	1440580693),
(NULL,	37,	'192.168.0.41',	'увыв',	'выва',	'выва',	1440580705),
(NULL,	38,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580860),
(NULL,	39,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580862),
(NULL,	40,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580863),
(NULL,	41,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580865),
(NULL,	42,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580867),
(NULL,	43,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580869),
(NULL,	44,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580870),
(NULL,	45,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580872),
(NULL,	46,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580874),
(NULL,	47,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580875),
(NULL,	48,	'192.168.0.41',	'qwe',	'dfgdfg',	'',	1440580877),
(NULL,	49,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580898),
(NULL,	50,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580900),
(NULL,	51,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580910),
(NULL,	52,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580911),
(NULL,	53,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580913),
(NULL,	54,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580915),
(NULL,	55,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580917),
(NULL,	56,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580919),
(NULL,	57,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580920),
(NULL,	58,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580922),
(NULL,	59,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580924),
(NULL,	60,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580926),
(NULL,	61,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580928),
(NULL,	62,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580929),
(NULL,	63,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580931),
(NULL,	64,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580933),
(NULL,	65,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580939),
(NULL,	66,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580940),
(NULL,	67,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580942),
(NULL,	68,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580944),
(NULL,	69,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580946),
(NULL,	70,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580948),
(NULL,	71,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580950),
(NULL,	72,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580952),
(NULL,	73,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580953),
(NULL,	74,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580955),
(NULL,	75,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580956),
(NULL,	76,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580958),
(NULL,	77,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580960),
(NULL,	78,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580962),
(NULL,	79,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580963),
(NULL,	80,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580965),
(NULL,	81,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580966),
(NULL,	82,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580968),
(NULL,	83,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580970),
(NULL,	84,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580971),
(NULL,	85,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580973),
(NULL,	86,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580974),
(NULL,	87,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580976),
(NULL,	88,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580978),
(NULL,	89,	'192.168.0.41',	'qwe',	'qwe',	'',	1440580979),
(NULL,	90,	'192.168.0.41',	'qwe',	'qwe',	'gfds',	1440580981),
(NULL,	91,	'192.168.0.41',	'qwe',	'qwe',	'gfds',	1440580983),
(NULL,	92,	'192.168.0.41',	'qwe',	'qwe',	'gfds',	1440580984),
(NULL,	93,	'192.168.0.41',	'qwe',	'qwe',	'gfds',	1440580986),
(NULL,	94,	'192.168.0.41',	'qwe',	'qwe',	'gfds',	1440580987),
(NULL,	95,	'192.168.0.41',	'qwe',	'qwe',	'gfds',	1440580989),
(NULL,	96,	'192.168.0.41',	'qwe',	'qwe',	'gfds',	1440580991),
(NULL,	97,	'192.168.0.41',	'qwe',	'qwe',	'gfds',	1440580992),
(NULL,	98,	'192.168.0.41',	'qwe',	'qwe',	'gfds',	1440580994),
(NULL,	99,	'192.168.0.41',	'qwe',	'qwe',	'gfds',	1440580995),
(NULL,	101,	'192.168.0.77',	'qwe',	'1234',	'adaad',	1441202949),
(NULL,	102,	'192.168.0.77',	'qwe',	'1234',	'adaad',	1441202952),
(NULL,	103,	'192.168.0.77',	'123',	'1234',	'12345',	1441203144),
(NULL,	104,	'192.168.0.77',	'123',	'1234',	'12345',	1441203146),
(NULL,	105,	'192.168.0.77',	'09',	'9',	'8',	1441203196),
(NULL,	106,	'192.168.0.77',	'09',	'9',	'8',	1441203197),
(NULL,	107,	'192.168.0.77',	'qwe',	'1234',	'adaadadaadadadadadadada',	1441203624),
(NULL,	108,	'192.168.0.77',	'asdasdad',	'234234234',	'sfssfsf',	1441203729),
(NULL,	109,	'192.168.0.77',	'asdasdad',	'234234234',	'sfssfsf',	1441203735),
(NULL,	110,	'192.168.0.77',	'qweqwe',	'1231',	'adeadsad',	1441203939),
(NULL,	111,	'192.168.0.77',	'954564564',	'956495649847',	'984984984',	1441204146),
(NULL,	112,	'192.168.0.77',	'954564564',	'956495649847',	'984984984',	1441204148),
(NULL,	113,	'192.168.0.77',	'954564564',	'956495649847',	'984984984',	1441204178),
(NULL,	114,	'192.168.0.77',	'954564564',	'956495649847',	'984984984',	1441204180),
(NULL,	115,	'192.168.0.77',	'sadasd',	'43543',	'fwfwef',	1441204239),
(NULL,	116,	'192.168.0.28',	'рфрфрфрфффрфр',	'12121212121212',	'йцйцйцйцйцййцйц',	1441619355),
(NULL,	117,	'192.168.0.28',	'рфрфрфрфффрфр',	'12121212121212',	'йцйцйцйцйцййцйц',	1441619381),
(NULL,	118,	'192.168.0.28',	'рфрфрфрфффрфр',	'12121212121212',	'йцйцйцйцйцййцйц',	1441619488),
(NULL,	119,	'192.168.0.28',	'qwe',	'1234',	'qweqweqweqweqwe',	1441620238),
(NULL,	120,	'192.168.0.28',	'sadsa',	'123',	'adaadaadadadad',	1441620551),
(NULL,	121,	'192.168.0.28',	'qwe',	'1234',	'фвыффывфыфывфвф',	1441620681),
('Нижняя форма',	123,	'192.168.0.28',	'qqqqqq',	'101010101010',	'qqqwwwweeeee',	1441620986),
('Нижняя форма',	124,	'192.168.0.28',	'йцукен',	'4444',	'йцукен',	1441621049),
('Всплывающее окно',	125,	'192.168.0.28',	'qweqwe',	'5555555555553333333333333',	'dadaadadadad',	1441621716);

DROP TABLE IF EXISTS `info`;
CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `info` (`id`, `description`) VALUES
(1,	'<p><span style=\"font-size:14px\"><span style=\"color:#FF0000\"><u>Я приобрел прогноз и ставка не сыграла &ndash; что делать?</u></span></span></p>\r\n\r\n<p><span style=\"font-size:14px\">Главное &ndash; не волноваться! Проигрыши &ndash; это часть нашей профессии. Проигрыши делают нас сильнее, мудрее, закаляют нас в профессиональном плане. Важно помнить, что заработок на ставках &ndash; это работа на дистанцию. Старайтесь ставить ежедневно не более 10% от банка на наши прогнозы &ndash; и по итогам месяца вы будете в хорошем плюсе. Просто взгляните на страницу статистики и убедитесь в этом сами!</span></p>\r\n');

DROP TABLE IF EXISTS `main`;
CREATE TABLE `main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `main` (`id`, `name`, `body`) VALUES
(1,	'Главная',	'<p>123</p>\r\n');

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `z_index` int(11) NOT NULL,
  `body` text NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu` (`id`, `name`, `z_index`, `body`, `parent_id`) VALUES
(1,	'Главная',	0,	'<p style=\"margin-left:20px; text-align:justify\"><img alt=\"\" src=\"/uploads/ckeditor/f499345f146140d4e2e735cab414.JPG\" style=\"float:left; height:400px; margin:7px 7px 7px 0px; width:267px\" /></p>\r\n\r\n<p style=\"text-align:justify\">Человек всегда хотел заглянуть в свое будущее.&nbsp;<br />\r\nВо всех мировых культурах можно найти&nbsp; различные виды<strong>предсказаний</strong>.&nbsp;<br />\r\nИх цель &ndash;&nbsp; увидеть&nbsp; грядущие события и ситуации, чтобы потом по возможности избежать многих неприятностей и хотя бы слегка обеспечить благополучие и стабильность в жизни.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Гадание</strong>&nbsp;было&nbsp; значительным элементом всех древних верований и религий. Есть свидетельства, что в течение сотен лет предсказания на<strong>картах Таро</strong>&nbsp;были востребованным средством получения информации по интересующим людей вопросам.</p>\r\n\r\n<p style=\"text-align:justify\">Многие виды искусства&nbsp;<strong>предсказания будущего</strong>&nbsp;передавались из одного&nbsp; поколения в другое. Наиболее известна такая предсказательная система, как&nbsp;<strong>карты Таро</strong>.</p>\r\n\r\n<p style=\"text-align:justify\">Каждый из нас, наверное, хотя бы раз слышал о магических картах Таро. Возникновение карт окутано тайной, никто точно не знает, откуда они взялись, существуют только догадки и красивые легенды.</p>\r\n\r\n<p style=\"text-align:justify\">Что можно узнать с помощью карт Таро, на какие вопросы получить ответы? Да практически на любой вопрос карты Таро могут дать ответ, посоветовать, как поступить в той или иной ситуации.</p>\r\n\r\n<p style=\"text-align:justify\">Конечно, наиболее всего людей волнуют вопросы&nbsp;<strong>взаимоотношений</strong>&nbsp;с другим человеком, стоит ли с ним дальше строить отношения, чего от него можно ожидать, не завел ли он (она) себе другую. А&nbsp; может быть&nbsp; вообще не стоит даже начинать эти отношения, потому что ничего, кроме боли и обид они не принесут?</p>\r\n\r\n<p style=\"text-align:justify\">Кстати, мне задают вопрос, можно ли&nbsp;<strong>гадать</strong>&nbsp;на иностранца. Конечно, можно. Производить гадание можно на человека любой национальности, вероисповедания и живущего в любом уголке земли. Для карт это не имеет значения.</p>\r\n\r\n<p style=\"text-align:justify\">Ответят карты Таро и на вопросы, связанные с бизнесом: чего можно ожидать при его открытии, как будут вести себя конкуренты, чего опасаться, как будет развиваться ситуация в дальнейшем.</p>\r\n\r\n<p style=\"text-align:justify\">С помощью карт Таро можно четко увидеть, какое на человеке имеется<strong>магическое воздействие</strong>: порча, сглазы, привороты, проклятья, подселенцы, импланты, в каком состоянии находится его защита и энергетика.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">Карты посоветуют, что нужно делать в этом случае, как избавиться от этого &laquo;добра&raquo;, которое на вас повесили &laquo;добрые люди&raquo;.</p>\r\n\r\n<p style=\"text-align:justify\">Вы можете получить ответ и о состоянии здоровья, но этот вид гадания<strong>тарологи</strong>&nbsp;выполняют неохотно. Оно и понятно: это большая ответственность. А умный таролог должен работать, как и врач, по принципу: &laquo;Не навреди!&raquo;&nbsp;<br />\r\nНо если человеку предстоит какое-то серьезное лечение или операция, конечно, ему хочется знать, будет ли от этого толк.</p>\r\n\r\n<p style=\"text-align:justify\">Я практикую гадание на картах Таро уже много лет, достигла определённого уровня, что позволяет мне профессионально оказывать услуги по гаданию и предсказанию на Таро тем, кто ко мне обращается.<br />\r\nПрактикую как личный прием, так и гадание по телефону, по фотографии - то есть удалённое гадание без присутствия гадающего.</p>\r\n\r\n<p style=\"text-align:justify\">Удалённое гадание ничем не хуже, чем если бы вы сидели рядом со мной и видели сам процесс. Иногда это даже лучше, потому что гадающий не влияет своими мыслями, эмоциями, желаниями на карты, а значит и на результат более достоверный.</p>\r\n\r\n<p style=\"text-align:justify\">Система Таро способна помочь в решении многих жизненно важных вопросов, а я , таролог Галина Мирова, постараюсь&nbsp; помочь&nbsp; вам в этом.</p>\r\n',	0);

DROP TABLE IF EXISTS `meta_tags`;
CREATE TABLE `meta_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_keywords` text,
  `meta_title` text,
  `meta_description` text,
  `name_page` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `meta_tags` (`id`, `meta_keywords`, `meta_title`, `meta_description`, `name_page`) VALUES
(1,	'Ключевые слова\r\n',	'Федеральное Агенство Рекламных Решений',	'мета описание',	'Главная');

DROP TABLE IF EXISTS `portfolio`;
CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `date` varchar(16) DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `portfolio` (`id`, `title`, `date`, `name`) VALUES
(3,	'Привет!',	'10 мая 2014',	'Супер / Пупер / Кар'),
(4,	'Привет3',	'10 мая 2015',	'Супер / Пупер / Кар');

DROP TABLE IF EXISTS `portfolio_slide`;
CREATE TABLE `portfolio_slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_portfolio` int(11) NOT NULL,
  `preview` varchar(64) NOT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `portfolio_slide` (`id`, `id_portfolio`, `preview`, `position`) VALUES
(1,	3,	'4ef5062f61c0d79ba64a0b85428e.jpg',	0),
(2,	3,	'bdc39276a0a9e9dff92735116b20.jpg',	1),
(3,	3,	'b80c355d2230a12373f6b209550d.jpg',	2),
(4,	3,	'e494ab8957a25503208159e0007b.jpg',	3),
(5,	3,	'5ff6264c826f4f2a8723be353b67.jpg',	4),
(6,	3,	'32077521e630836d039449b11b83.jpg',	5),
(31,	4,	'78bf706810017b317354a91b7b4b.jpg',	0),
(32,	4,	'a822bca52286e8032b9cc4588b5c.jpg',	2),
(33,	4,	'0f901adf8f674ad5a6bf97bb60d0.jpg',	3),
(34,	4,	'647450d20efe07347d3bf98473cd.jpg',	4),
(35,	4,	'5f99b340817c0a7fd9959687d0fa.jpg',	5),
(36,	4,	'05b7b1471f1575ed82ca8977441f.jpg',	1);

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `date` varchar(32) NOT NULL,
  `sub_name` varchar(32) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` int(11) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `description_view_ru` text,
  `description_detail_ru` text,
  `image_view` varchar(32) NOT NULL,
  `image` varchar(32) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `project` (`date`, `sub_name`, `id`, `sort`, `name_ru`, `description_view_ru`, `description_detail_ru`, `image_view`, `image`, `type`) VALUES
('Омск 2015',	'БТИ Омской области',	6,	1,	'БТИ',	NULL,	'БТИ Омской области БТИ Омской области БТИ Омской области',	'5a4e79aaeebc5e6fbbfd07e358b5.jpg',	'3f29135a6a3aac4914f0f3bea14f.jpg',	''),
('2014 Новосибирск',	'Лэндинг для продажи систем полив',	7,	2,	'Системы автополива',	NULL,	'Лэндинг для продажи систем полива Лэндинг для продажи систем полива Лэндинг для продажи систем полива',	'e571fecb59e4110a42c812e4b5a9.jpg',	'3a3349a772515d129888c42d82bf.jpg',	''),
('2013 Омск',	'Лэндинг - аренда спецтехники',	8,	3,	'Аренда спецтехники',	NULL,	'Лэндинг - аренда спецтехники Лэндинг - аренда спецтехники Лэндинг - аренда спецтехники',	'332e809266c96120c328dac6d13f.jpg',	'03ada521b86842ea45e96dbd4f7c.jpg',	''),
('2016 Москва',	'Сайт лэндинг для Юриста в Омске',	9,	4,	'Юридические услуги 24',	NULL,	'Сайт лэндинг для Юриста в Омске Сайт лэндинг для Юриста в Омске Сайт лэндинг для Юриста в Омске',	'995f6f5e03132a0a0afa77b89585.jpg',	'0943ae0fc0d524ff57fd28ae274f.jpg',	'');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(16) NOT NULL,
  `password` varchar(64) NOT NULL,
  `date_create` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `login`, `password`, `date_create`) VALUES
(1,	'admin',	'$2y$13$HH/wJ25Vz4qnxUrT2LDGzODWFFx5PVYccxunt4qPAnVqB65RWBs5K',	10000);

-- 2015-09-07 13:48:47
